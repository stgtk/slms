# Stugütak Live Media System

Creating a media playback and live video system for theatrical productions and alike. This project should not only propagate open source in the field of theatre tech, slms aims to enable the use of live video in independent, «low-cost» (in contrast to the state funded houses) theater productions. The project was initially created 2019 for the Play [«kater – Oder wie das Leben der Welt den Rücken kehrte»](https://stgtk.ch).


## Aims

- Producing a replicable and cost-aware solution for live streaming video in stage plays.
- Documenting the setup and usage of the software. So other artists can adapt it.
- Do not reinvent the wheel. Whenever possibleValues, we use existing solutions which are field tested.
- The implementation should be as agnostic as possibleValues, so there is room for further development.


## Architecture Overview

Slms takes control input from a GUI and triggers Targets according to a given cue list. The video architecture is based on the [GStreamer multimedia framework](https://gstreamer.freedesktop.org/) and relies heavily on [GstInterpipe](https://developer.ridgerun.com/wiki/index.php?title=GstInterpipe) by RidgeRun. We use [Groovy](http://groovy-lang.org/) as a programming language and [gst1-java-core](https://github.com/gstreamer-java/gst1-java-core)

### Software

The main target platform for slms is [Ubuntu](https://www.ubuntu.com/). But as it runs on the JVM, it should be possibleValues to run it on other systems.

Slms offers a number of pipelines, in which the video can be captured, altered and broadcasted in real time. For more performance, slms heavily uses the [OpenGl capabilities of GStreamer ](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-plugin-opengl.html). Internally slms works with a resolution of 1920 x 1080 («Full HD») at 30 Frames per Second and RGBA as color space.

![Schematic overview of the pipeline structure](misc/schematics/slms_overview.png)

For «kater» we implemented the following pipelines:

- Input
    - **MJPEG.** Get Videostreams from WebCams via [v4l2](https://linuxtv.org/downloads/v4l-dvb-apis/uapi/v4l/v4l2.html).
    - **H.256.** Also for WebCams.
    - **Blackmagic DeckLink.** Gets the video from a [Blackmagic DeckLink](https://www.blackmagicdesign.com/products/decklink).
    - **File.** Opens and plays video files. Default input Format: Container: QuickTime, Codec: [ProRes 4444 XQ](https://support.apple.com/en-gb/HT202410), 1920 x 1080, 30 Frames/second.
    - **Test Image.** All testscreens provided by GStreamers [videotestsrc](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-videotestsrc.html).
- Trough
    - **Default.** Provides the capabilities to insert [OpenGL Shaders](https://en.wikipedia.org/wiki/Shader), fading and mixing of multiple input streams.
- Out
    - **GlImageSink.** Outputs a video stream on a OpenGL window.
    - **XVImageSink.** Outputs a video stream on a X window.
    - **Blackmagic DeckLink.** Output via a Blackmagic DeckLink card.

## Backing and subsidy

We want to thank all organizations, which supported «kater» and the development of slms.

## Sponsors

### [Förderfonds Berner Kantonalbank](https://www.bekb.ch/de/die-bekb/engagement/bekb-foerderfonds)

<img width="380px" src="doc/assets/bekb.png"/>


### [Burgergemeinde Bern](https://www.bgbern.ch/)

<img width="380px" src="doc/assets/bgb.png"/>


### [Kulturstiftung Gebäudeversicherung Bern](https://www.gvb.ch/de/home/)

<img width="380px" src="doc/assets/gvb.png"/>


### [Gesellschaft zu Ober-Gerwern](https://www.ober-gerwern.ch/home.html)

<img height="150px" src="doc/assets/obergerwern.png"/>


### [Startstutz (City of Bern)](https://startstutz.ch/)

<img height="150px" src="doc/assets/startstutz.png"/>


## Support and infrastructure

### [BeST – Student Theatre of Bern](https://bestheater.ch)

<img width="380px" src="doc/assets/best.png"/>


### [Genossenschaft Solutionsbüro](https://buero.io)

<img height="150px" src="doc/assets/sb.png"/>
