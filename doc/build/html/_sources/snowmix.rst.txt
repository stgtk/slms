Snowmix
=======

Installation
------------

TODO: Alles schön formatieren

Herunterladen

Entpacken

Bootstrapscript anpassen (unter bootstrapd/bootstrap-ubuntu). libpng12-dev -> libpng-dev 2x linien

./bootstrap

Zu ``.bashrc`` hinzufügen::

  export SNOWMIX=/usr/local/lib/Snowmix-0.5.1

GStreamer Input
---------------

Die entsprechende Dokumentation gibts [hier](https://snowmix.sourceforge.io/Examples/input.html). Dieser [Forumsbeitrag](https://sourceforge.net/p/snowmix/discussion/Snowmix_Support_Forum/thread/5df38b3a/) beschäftigt sich mit der DeckLink Karte im Speziellen.

GStreamer Output
----------------

Output ist eine grüne Fläche: Die angegebene Breite und Höhe entspricht nicht der Ausgabe von Snowmix
