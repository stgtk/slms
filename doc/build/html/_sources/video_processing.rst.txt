Video
*****

Defaults
========

As we need the support of alpha channels in videos, we have to use ProRes.

+-----------+----------------+
| Object    | Value          |
+===========+================+
| Width     | 1920px         |
+-----------+----------------+
| Heigth    | 1080px         |
+-----------+----------------+
| Framerate | 30fps          |
+-----------+----------------+
| Container | QuickTime      |
+-----------+----------------+
| Codec     | ProRes 4444 XQ |
+-----------+----------------+

Generate ProRes 4444 Alpha files
================================

Blender
-------

Build scene and export with the following settings:


``Properties Editor > Render Context > Dimensions Panel``
  Resolution: X: 1920, Y: 1080, 100%
``Properties Editor > Render Context > Output Panel``
  File Type: TIFF
  Colorspace: RGBA
``Properties Editor > Render Context > Film Panel``
  Transparent: True


FFmpeg
------

::

  ffmpeg -f image2 -framerate 30 -i %04d.tif -vcodec prores_ks -pix_fmt yuva444p10le -profile:v 5 output.mov


Profiles:

+------+-----------------------------+
| Code | Desciption                  |
+======+=============================+
| 5    | 4444 XQ                     |
+------+-----------------------------+
| 4    | 4444                        |
+------+-----------------------------+
| 3    | HQ (high quality)           |
+------+-----------------------------+
| 2    | Standard (rarely called ST) |
+------+-----------------------------+
| 1    | LT (light)                  |
+------+-----------------------------+
| 0    | Proxy (sometimes called PR) |
+------+-----------------------------+
