Veraltete Informationen
***********************

Diese Informationen sind für die aktuelle Version von slms nicht mehr relevant.

ffmpeg
======

Ffmpeg ist für das En- und Decoding der Videostreams verantwortlich. Insbesondere encoded es den SDI Input auf der DeckLink Karte und übergibt es über TCP dem voctocore.

ffmpeg und DeckLink
-------------------

Informationen finden sich grundsätzlich in der `<Dokumentation von ffmpeg https://www.ffmpeg.org/ffmpeg-devices.html#decklink>`. Wichtig: Die DeckLink Karten sind betreffend der unterstützten Formate sehr wählerisch.

Alle verfügbaren DeckLink Devices auflisten. Erster Hinweis: Jeder Ein-/Ausgang einer DeckLink Karte ist für ffmpeg ein eigenes Device. Zweiter Hinweis: Die verfügbaren Devices können auch im BlackMagic Desktop Video Setup Programm unter *Label* eingesehen werden.::

  ffmpeg -f decklink -list_devices 1 -i dummy

Die verschiedenen Eingangsformate für ein Device auflisten. Dies ist sehr wichtig, da die Karten, wie schon erwähnt, sehr wählerisch sind. (Parameter hinter ``-i`` entsprechend anpassen)::

  ffmpeg -f decklink -list_formats 1 -i 'DeckLink Duo (2)'


Snowmix
=======

Installation
------------

Herunterladen

Entpacken

Bootstrapscript anpassen (unter bootstrapd/bootstrap-ubuntu). libpng12-dev -> libpng-dev 2x linien

./bootstrap

Zu ``.bashrc`` hinzufügen::

  export SNOWMIX=/usr/local/lib/Snowmix-0.5.1

GStreamer Input
---------------

Die entsprechende Dokumentation gibts `hier <https://snowmix.sourceforge.io/Examples/input.html>`_. Dieser `Forumsbeitrag <https://sourceforge.net/p/snowmix/discussion/Snowmix_Support_Forum/thread/5df38b3a>`_ beschäftigt sich mit der DeckLink Karte im Speziellen.

GStreamer Output
----------------

Output ist eine grüne Fläche: Die angegebene Breite und Höhe entspricht nicht der Ausgabe von Snowmix
