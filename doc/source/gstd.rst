GStreamer Daemon / Interpipes
*****************************

Die vollständige Dokumentation findet sich `hier <https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon>`_. Die Befehle sind teilweise sehr spezifisch auf unser Setup und müssen dementsprechend angepasst werden.

Prevent Memory Leaks
====================



Allgemein
=========

Pipeline hinzufügen
-------------------

::

	gstd-client pipeline_create [PIPELINE_NAME]


Pipeline starten
----------------

::

	gstd-client pipeline_play [PIPELINE_NAME]


Pipeline löschen
----------------

::

	gstd-client pipeline_delete [PIPELINE_NAME]

Listen to Pipeline wechseln
---------------------------

::

    gstd-client element_set auto_sink interpipesrc1 listen-to src_2


Sources
=======

Webcam
------

Mit v4l2 ::

	gstd-client pipeline_create pipe_1_src v4l2src device=/dev/video0 ! "video/x-raw, framerate=10/1, width=1280, height=720" ! queue ! interpipesink name=src_1 caps=video/x-raw,width=1280,height=720,framerate=10/1 sync=false async=false


Videotestsrc
------------

::

	gstd-client pipeline_create pipe_2_src videotestsrc pattern=snow is-live=true ! "video/x-raw, framerate=10/1, width=1280, height=720" ! queue ! interpipesink name=src_2 caps=video/x-raw,width=1280,height=720,framerate=10/1 sync=false async=false


Canon Legria HF R17
-------------------

Via HDMI -> BlackMagic HDMI to SDI -> SDI -> DeckLink ::

	gstd-client pipeline_create pipe_1_src \
	  decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
	  deinterlace ! videorate ! videoconvert ! \
	  'video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
	  queue ! \
	  interpipesink name=decklink_src \
	  sync=false async=false


Raspberry PI
------------

Via HDMI -> BlackMagic HDMI to SDI -> SDI -> DeckLink ::

	gstd-client pipeline_create pipe_1_src \
	  decklinkvideosrc mode=13 device-number=0 connection=1 name=black ! \
	  deinterlace ! videorate ! videoconvert ! \
	  'video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
	  queue ! \
	  interpipesink name=decklink_src \
	  sync=false async=false

Sinks
=====

Autovideosink
-------------

Praktisch fürs Debuggen ::

	gstd-client pipeline_create pipe_4_sink interpipesrc name=interpipesrc1 listen-to=src_1 is-live=true allow-renegotiation=true enable-sync=true ! queue ! videoconvert ! autovideosink async=false sync=false
