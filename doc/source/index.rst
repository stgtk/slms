.. slms documentation master file, created by
   sphinx-quickstart on Wed Jan 23 00:53:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Stgtk Live Media System
=======================

**Diese Dokumentation befindet sich im Aufbau und ist darum weder vollständig noch fehlerfrei.**


.. toctree::
   :maxdepth: 2
   :caption: Installation

   hardware_setup
   server_setup


.. toctree::
   :maxdepth: 2
   :caption: SLMS

   slms

.. toctree::
   :maxdepth: 2
   :caption: Tools and Services

   gst
   gstd
   video_processing

.. toctree::
   :maxdepth: 2
   :caption: Development

   slms_architecture
   latency
   deprecated
   cams


Danksagung
==========

Wir danken folgenden Projekten:

- `gstreamer <https://gstreamer.freedesktop.org/>`_
- `gstreamer daemon <https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon>`_ und `gstInterpipe <https://developer.ridgerun.com/wiki/index.php?title=GstInterpipe>`_
-


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
