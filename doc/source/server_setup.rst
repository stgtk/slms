Installation Server
*******************

Der Server ist das Herzstück des Systems. Er übernimmt das Encoding und Mixen der Videostreams. Die Installation erfordert ein Grundwissen über die Bedienung von Linux und der Bash.

Betriebssystem
==============

Wir arbeiten mit `Ubuntu <https://www.ubuntu.com/>`_ Version 18.10 und haben sehr gute Erfahrungen gemacht. Da gewisse Programme nur über eine grafische Benutzeroberfläche steuerbar sind, empfiehlt sich die Installation von Ubuntu Desktop. Weitere Informationen über die Installation von Ubuntu findet sich auf der entsprechenden Website oder auf `ubuntuusers.de <https://wiki.ubuntuusers.de/Ubuntu_Installation/>`_.

Desktop Video
=============

Damit die DeckLink Karte benutzt werden kann, muss das dazugehörige Programmpaket von Blackmagic installiert werden. Es müssen zunächst noch folgende Pakete installiert werden::

  sudo apt-get install \
    dkms \


Dann auf die `Supportwebsite <https://www.blackmagicdesign.com/support/family/capture-and-playback>`_ gehen und die aktuellste Version von *Desktop Video* herunterladen.

Das Archiv entpacken::

  tar -xzf Blackmagic_Desktop_Video_Linux_*.tar.gz

Dann ::

  cd Blackmagic_Desktop_Video_Linux_*
  sudo dpkg -i deb/x86_64/desktopvideo_*
  sudo dpkg -i deb/x86_64/desktopvideo-gui_*
  sudo dpkg -i deb/x86_64/desktopvideo-scanner_*.deb
  sudo dpkg -i deb/x86_64/mediaexpress_*.deb

Sollte die Installation fehlschlagen, so kann dieses Problem manchmal mit folgendem Befehl gelöst werden::

  sudo apt --fix-broken install

GStreamer
=========

GStreamer installieren. ::

  sudo apt-get install \
    libgstreamer1.0-0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    gstreamer1.0-doc \
    gstreamer1.0-tools \
    gstreamer1.0-x \
    gstreamer1.0-alsa \
    gstreamer1.0-gl \
    gstreamer1.0-gtk3 \
    gstreamer1.0-pulseaudio


GstInterpipe
============

Slms benötigt GstInterpipe, ein GStreamerplugin, welches in der Standartinstallation nicht enthalten ist. Weiterführende Informationen über die Installation finden sich in der `Wiki des Herstellers <https://developer.ridgerun.com/wiki/index.php?title=GstInterpipe_-_Building_and_Installation_Guide>`_.


gtk-doc
-------

Die in den Paketquellen von Ubuntu enthaltene Version von gtk-doc entspricht nicht den Anforderungen von gstd. Darum muss es manuell installiert werden.

Zunächst die Dependencies installieren::

  sudo apt-get install \
    docbook-xml \
    docbook-xsl \
    gnome-doc-utils \
    itstool

Dann unter `<https://download.gnome.org/sources/gtk-doc/>`_ die aktuelle Version von gtk-doc herunterladens und das tar entpacken.::

  tar xf gtk-doc-[VERSION].tar.xz

In das Verzeichnis wechseln, compillieren und installieren::

  cd gtk-doc-[VERSION]
  ./configure
  make
  sudo make install


Weitere Abhängigkeiten
----------------------

Es müssen noch folgende Abhängigkeiten installiert werden::

  sudo apt-get install \
    git \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev


GstInterpipe
------------

Herunterladen.::

  git clone https://github.com/RidgeRun/gst-interpipe.git

Für die Installation ist das Perl Script von Nöten, welches nicht mitgeliefert wird. Dieses kann etwa `hier <https://gitlab.gnome.org/GNOME/gtk-doc/blob/4c7a75b7127e32020cf58ca1a1675adc24bfd54b/gtkdoc-common.pl.in>`_ heruntergeladen werden. Die Datei danach unter dem Namen ``gtkdoc-common.pl`` unter ``/usr/local/share/gtk-doc/data/`` abspeichern. **Achtung: Dies ist ein Workaround und keine nachhaltige Lösung.**


In das Verzeichnis wechseln, compillieren und installieren.::

  cd gst-interpipe
  ./autogen.sh --libdir /usr/lib/x86_64-linux-gnu/gstreamer-1.0/
  make
  sudo make install

Die Installation testen. Das Terminal sollte eine Beschreibung des Plugins zurückgeben.::

  gst-inspect-1.0 interpipe


GStreamer Daemon
================

Der Videomixer von slms basiert auf dem GStreamer Daemon (gstd). Weitere Informationen zur Installation finden sich auf der `Wiki des Herstellers <https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon_-_Building_GStreamer_Daemon>`_

Abhängigkeiten
--------------

Folgende Abhängigkeiten installieren::

  sudo apt-get install \
    automake \
    libtool \
    pkg-config \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    libglib2.0-dev \
    libjson-glib-dev \
    gtk-doc-tools \
    libreadline-dev \
    libncursesw5-dev \
    libdaemon-dev


GStreamer Daemon
----------------

Herunterladen.::

  git clone https://github.com/RidgeRun/gstd-1.x.git

In das Verzeichnis wechseln, compillieren und installieren.::

  cd gstd-1.x
  ./autogen.sh
  ./configure
  make
  sudo make install

Die Installation testen. Beide Befehle sollten ausführbar sein.::

  gstd
  gstd-client


Slms
====

Hiermit ist die Installation von slms abgeschlossen. Die nun folgenden Schritte sind nur für fortgeschrittene Benutzer und Entwickler interessant und fügen slms keine weitere Funktionalität hinzu.

ffmpeg
======

**Dieser Schritt ist optional. Smls funktioniert ohne ffmpeg.**

Damit `ffmpeg <https://ffmpeg.org/>`_ mit den DeckLink Karten umgehen kann, muss das Programm selbst kompilliert werden. Für weitere Informationen empfiehlt sich einen Blick in die `compilation Guide <https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu>`_ von ffmpeg.

Dependencies
------------

Zuerst die benötigten Packete installieren ::

  sudo apt-get update -qq && sudo apt-get -y install \
    autoconf \
    automake \
    build-essential \
    cmake \
    git-core \
    libass-dev \
    libfreetype6-dev \
    libsdl2-dev \
    libtool \
    libva-dev \
    libvdpau-dev \
    libvorbis-dev \
    libxcb1-dev \
    libxcb-shm0-dev \
    libxcb-xfixes0-dev \
    pkg-config \
    texinfo \
    wget \
    zlib1g-dev

Ordner
------

Die Ordner für die Installation erstellen::

  mkdir -p ~/ffmpeg_sources ~/bin

Third-Party Bibliotheken
------------------------

Es können eine Reihe von Third-Party Bibliotheken installiert werden, damit ffmpeg diese Codecs usw. unterstützt. Dies ist zwar nicht zwingend nötig, der Einfachheit halber, geht die Dokumentation aber davon aus, dass alle installiert werden sollten. Wer was anderes will, muss alle folgenden Befehle selbst anpassen.

Die bennötigten Packete installieren::

  sudo apt-get install nasm
  sudo apt-get install yasm
  sudo apt-get install libx264-dev
  sudo apt-get install libx265-dev libnuma-dev
  sudo apt-get install libvpx-dev
  sudo apt-get install libfdk-aac-dev
  sudo apt-get install libmp3lame-dev
  sudo apt-get install libopus-dev

Da libaom nicht in den Paketquellen von Ubuntu vorhanden ist, muss dieses selbst compiliert werden::

  cd ~/ffmpeg_sources && \
  git -C aom pull 2> /dev/null || git clone --depth 1 https://aomedia.googlesource.com/aom && \
  mkdir -p aom_build && \
  cd aom_build && \
  PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED=off -DENABLE_NASM=on ../aom && \
  PATH="$HOME/bin:$PATH" make && \
  make install


DeckLink SDK
------------

Damit ffmpeg mit der Desklink Unterstützung compilliert werden kann, muss das DeckLink SDK installiert werden. Für den Download auf die `Entwicklerseite von BlackMagic <https://www.blackmagicdesign.com/developer/product/capture-and-playback>`_ gehen, runterscrollen und das *Desktop Video SDK* herunterladen.

Das Archiv entpacken und nun an einem geeigneten Ort speichern. Für diese Dokumentation gehen wir davon aus, dass die Daten unter ``/usr/src/ffmpeg/BMDL_SDK/`` liegen (also so, dass der ``include`` Ordner den Pfad ``/usr/src/ffmpeg/BMDL_SDK/Linux/include`` hat).

ffmpeg herunterladen & compilieren
----------------------------------

Zuerst ffmpeg herunterladen::

  cd ~/ffmpeg_sources
  wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
  tar xjvf ffmpeg-snapshot.tar.bz2

Dann das Compilieren starten. Damit die ganze Geschichte möglichst schnell geht, kann mit dem Flag ``-j[N OF CORES]`` angegeben werden, wie viele Kerne des Prozessors benutzt werden sollen. Die Anzahl bitte den entsprechenden Gegebenheiten anpassen.::

  PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
    --prefix="$HOME/ffmpeg_build" \
    --pkg-config-flags="--static" \
    --extra-cflags="-I$HOME/ffmpeg_build/include" \
    --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
    --extra-libs="-lpthread -lm" \
    --bindir="$HOME/bin" \
    --enable-gpl \
    --enable-libaom \
    --enable-libass \
    --enable-libfdk-aac \
    --enable-libfreetype \
    --enable-libmp3lame \
    --enable-libopus \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libx264 \
    --enable-libx265 \
    --enable-nonfree \
    --enable-decklink \
    --extra-cflags=-I/usr/src/ffmpeg/BMDL_SDK/Linux/include/ \
    --extra-ldflags=-L/usr/src/ffmpeg/BMDL_SDK/Linux/include/ && \
  PATH="$HOME/bin:$PATH" make -j[N OF CORES] && \
  make install -j[N OF CORES] && \
  hash -r

Zum Abschluss::

  source ~/.profile

Testen der Installation
-----------------------

Um die Installation zu testen, sollte folgender Befehl die verfügbaren SDI Eingänge aufführen::

  ffmpeg -f decklink -list_devices 1 -i dummy

Weitere Informationen über die Benutzung von ffmpeg finden sich hier.

voctocore
=========

**Dieser Schritt ist optional. Smls funktioniert auch ohne voctomix.**

Die benötigten Packete installieren::

  sudo apt-get install gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-tools libgstreamer1.0-0 python3 python3-gi gir1.2-gstreamer-1.0 gir1.2-gst-plugins-base-1.0
