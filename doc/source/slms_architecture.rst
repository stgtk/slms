Architektur Slms
****************

**Depreciated**

Dieses Dokument beschreibt den technischen Aufbau der slms. Für die Bedienung von slms ist dieses Wissen im Normalfall nicht von Nöten.

Videomixer
==========

Der Videomixer baut auf `gstreamer <https://gstreamer.freedesktop.org/>`_ auf. Um die Benutzung zu vereinfachen verwenden wir `gstreamer daemon <https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon>`_ und die Möglichkeiten von `Interpipes <https://developer.ridgerun.com/wiki/index.php?title=GstInterpipe>`_. Slms steuert den Mixer über TCP.

Features
--------

Folgende Funktionen hat der Videomixer:

- Input

   - Input aus DeckLink Karten (``cam_src``).
   - Input aus Videofiles (``file_src``).
   - Schwarzes Bild (``black_src``).
   - Testbilder (``test_src``)
- Effekte

   - Alpha und Chromakey. (Beides über das alpha Plugin implementiert)
   - Eventuell: Weitere Effekte
- Output

   - Fenster mit X
   - Optional: Output über DeckLink Karte
   - Optional: Direkt in Framebuffer schreiben (fbdesink)
- Debugging. Ein X Fenster für jede Inputsource
- Ausgabe

   - Eine Source im Vollbild
   - Mehrere Sources in Grösse und Position verändern, mit Alpha übereinander Legen (videobox und videomixer)

Implementation
--------------

.. image:: static/videomixer_pipelines.png

Input Pipes
^^^^^^^^^^^

Es gibt vier verschiedene Videoquellen und dem entsprechend vier verschiedene Input Pipes:

``cam_src``
    Für Video, welches mit der Blackmagic DeckLink Karte aufgezeichnet wird. Hierfür wird `decklinkvideosrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-bad/html/gst-plugins-bad-plugins-decklinkvideosrc.html>`_ verwendet. Snippets für die Benutzung finden sich bei :ref:`gstreamer-decklink`.
``file_src``
    Input aus Videodateien. Verwendet `filesrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer-plugins/html/gstreamer-plugins-filesrc.html>`_. Snippets finden sich unter :ref:`gstreamer-file`.
``black_src``
    Ein schwarzes Bild. Hiermit kann zum einen die Videoausgabe «gemutet» werden, zum Anderen wird diese Source Pipe verwendet, wenn eine andere ausfällt. Verwendet `videotestsrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-videotestsrc.html>`_. Snippet finden sich unter :ref:`gstreamer-black`.
``test_src``
    Dient zum Testen und Entwicklen, wenn die reale Videoquelle nicht zur Verfügung steht. Verwendet `videotestsrc <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-videotestsrc.html>`_. Snippet finden sich unter :ref:`gstreamer-test`.

Mithilfe von `videoconvert <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-videoconvert.html>`_ werden die unterschiedlichen Quellen in das selbe interne Format umgewandelt. Die Caps hierzu::

    video/x-raw,
    format=BGRA,
    framerate=30/1,
    width=1920,
    height=1080


Effect Pipes
^^^^^^^^^^^^

Jeder Input Pipe ist eine Effect Pipe zugeordnet. Obwohl diese momentan nur über einen Effekt verfügen (`alpha <https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good/html/gst-plugins-good-plugins-alpha.html>`_), ist dies als eigene Pipe implementiert, damit Später einfach neue Effekte eingebaut werden können.
