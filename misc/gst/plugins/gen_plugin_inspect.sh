#!/usr/bin/env bash

plugins=(
    "glcolorbalance"
    "glcolorconvert"
    "glcolorscale"
    "gldeinterlace"
    "gldifferencematte"
    "gldownload"
    "glshader"
    "glupload"
    "autovideosink"
    "glimagesink"
    "decklinkvideosrc"
    "v4l2src"
    "videotestsrc"
    "alpha"
)

for plugin in "${plugins[@]}"
do
    gst-inspect-1.0 --print-plugin-auto-install-info ${plugin} > ${plugin}.txt
done