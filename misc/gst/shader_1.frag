#version 100
#ifdef GL_ES
precision mediump float;
#endif
varying vec2 v_texcoord;
uniform sampler2D tex;
uniform float time;
uniform float width;
uniform float height;

void main() {
  vec2 temp = v_texcoord;

  for (int i = 3; i >= 1; i--) {
    float factor = float(i);
    if (v_texcoord.x > 0.25 * factor) {
      temp.x = v_texcoord.x - 0.25 * factor;
      break;
    }
  }

  for (int i = 3; i >= 1; i--) {
    float factor = float(i);
    if (v_texcoord.y > 0.25 * factor) {
      temp.y = v_texcoord.y - 0.25 * factor;
      break;
    }
  }

  gl_FragColor = texture2D(tex, temp);
}
