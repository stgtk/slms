#!/bin/bash

gstd-client pipeline_create test_src_1 \
  videotestsrc ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  interpipesink name=src_1 forward-events=true sync=true

gstd-client pipeline_create test_src_2 \
  videotestsrc is-live=true pattern=ball ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! \
  interpipesink name=src_2 forward-events=true sync=true

gstd-client pipeline_create decklink_pipe \
  decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! \
  deinterlace ! videorate ! videoconvert ! \
  'video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, interlace-mode=interleaved, framerate=30/1, width=1920, height=1080' ! \
  queue ! \
  interpipesink name=decklink_src forward-events=true sync=true

gstd-client pipeline_create test_src_3 \
  videotestsrc ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=800, height=600 ! \
  interpipesink name=src_3 forward-events=true

gstd-client pipeline_create test \
  videotestsrc pattern=snow ! \
  video/x-raw,format=BGRA,width=800,height=600,framerate=30/1 ! \
  timeoverlay ! \


interpipesink name=src_test forward-events=true

gstd-client pipeline_create pipeline_video_mixer \
  interpipesrc listen-to=decklink_src is-live=true format=time ! queue ! \
  videomixer name=mixer background=black sink_0::xpos=200 sink_1::alpha=0.6 ! \
  video/x-raw, format=BGRA, pixel-aspect-ratio=1/1, framerate=30/1, width=1920, height=1080 ! interpipesink name=the_mix \
  \
  interpipesrc listen-to=src_2 is-live=true format=time ! queue ! mixer.

gstd-client pipeline_create auto_sink \
  interpipesrc name=interpipesrc1 listen-to=the_mix is-live=true allow-renegotiation=true ! \
  queue ! \
  videoconvert ! \
  autovideosink async=false sync=false

gstd-client pipeline_play test_src_1
gstd-client pipeline_play decklink_pipe
gstd-client pipeline_play test_src_2
gstd-client pipeline_play pipeline_video_mixer
gstd-client pipeline_play auto_sink

# gstd-client element_set auto_sink interpipesrc1 listen-to src_2
