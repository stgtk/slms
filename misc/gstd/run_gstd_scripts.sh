#!/bin/bash

# This script starts the gstreamer deamon, runs a given script and kills gstreamer deamon upon a user command.

echo "Start gstd"
# ./timeout -m 500000 'gstd &'
gstd &
sleep 0.1s
echo "Start " $1
bash $1
read -p "Press enter to quit"
# sleep 5s
killall gstd
