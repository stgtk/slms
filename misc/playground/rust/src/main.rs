mod pipeline;

fn main() {
    let element = pipeline::gstreamer.gstProperties.GstFraction {
        numerator: 30,
        denominator: 1,
    };

    println!("{}", element.render_value());

}
