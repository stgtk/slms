pub trait GstValue {
    fn render_value(&self) -> String;
    fn set_value();
}

pub struct gstreamer.gstProperties.GstFraction {
    numerator: i32,
    denominator: i32,
}

impl gstreamer.gstProperties.GstFraction {
    pub fn render_value(&self) -> String {
        format!("(fraction){}/{}", self.numerator, self.denominator)
    }
}


pub struct gstreamer.gstProperties.GstString {
    pub value
}