package gstreamer

abstract class SGstElement {
    enum GstElementType {
        SRC,
        TROUGH,
        SINK
    }

    String name
    GstElementType elementType

    SGstElement(GstElementType elementType) {
        this.elementType = elementType
    }

    abstract String renderElement()
}
