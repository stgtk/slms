package gstreamer.gstCaps

import gstreamer.gstProperties.GstProperty

@SuppressWarnings(["SpellCheckingInspection", "SpellCheckingInspection", "SpellCheckingInspection", "SpellCheckingInspection", "unused"])
class GstCapsType extends GstProperty<String> {
    enum TypeCategory {
        AUDIO,
        VIDEO,
        IMAGE,
        CONTAINER,
    }

    TypeCategory typeCategory

    GstCapsType(String key, TypeCategory typeCategory, String name, String description = "") {
        super(key, description)
        this.typeCategory = typeCategory
        this.key = "type"
        this.value = name
    }

    @Override
    String renderValue() {
        this.getValue()
    }

    static final GstCapsType audioRaw = new GstCapsType("audioRaw", TypeCategory.AUDIO, "audio/x-raw", "Unstructured and uncompressed raw audio data.")
    static final GstCapsType audioAc3A52 = new GstCapsType("audioAc3A52", TypeCategory.AUDIO, "audio/x-ac3", "AC-3 or A52 audio streams.")
    static final GstCapsType audioAdpcm = new GstCapsType("audioAdpcm", TypeCategory.AUDIO, "audio/x-adpcm", "ADPCM Audio streams.")
    static final GstCapsType audioXinepak = new GstCapsType("audioXinepak", TypeCategory.AUDIO, "audio/x-cinepak", "Audio as provided in a Cinepak (Quicktime) stream.")
    static final GstCapsType audioDv = new GstCapsType("audioDv", TypeCategory.AUDIO, "audio/x-dv", "Audio as provided in a Digital Video stream.")
    static final GstCapsType audioFlac = new GstCapsType("audioFlac", TypeCategory.AUDIO, "audio/x-flac", "Free Lossless Audio codec (FLAC).")
    static final GstCapsType audioGsm = new GstCapsType("audioGsm", TypeCategory.AUDIO, "audio/x-gsm", "Data encoded by the GSM codec.")
    static final GstCapsType audioAlaw = new GstCapsType("audioAlaw", TypeCategory.AUDIO, "audio/x-alaw", "A-Law Audio.")
    static final GstCapsType audioMuLaw = new GstCapsType("audioMuLaw", TypeCategory.AUDIO, "audio/x-mulaw", "Mu-Law Audio.")
    static final GstCapsType audioMace = new GstCapsType("audioMace", TypeCategory.AUDIO, "audio/x-mace", "MACE Audio (used in Quicktime).")
    static final GstCapsType audioMpeg = new GstCapsType("audioMpeg", TypeCategory.AUDIO, "audio/mpeg", "Audio data compressed using the MPEG audio encoding scheme.")
    static final GstCapsType audioQdm2 = new GstCapsType("audioQdm2", TypeCategory.AUDIO, "audio/x-qdm2", "Data encoded by the QDM version 2 codec.")
    static final GstCapsType audioRealmedia = new GstCapsType("audioRealmedia", TypeCategory.AUDIO, "audio/x-pn-realaudio", "Realmedia Audio data.")
    static final GstCapsType audioSpeex = new GstCapsType("audioSpeex", TypeCategory.AUDIO, "audio/x-speex", "Data encoded by the Speex audio codec")
    static final GstCapsType audioVorbis = new GstCapsType("audioVorbis", TypeCategory.AUDIO, "audio/x-vorbis", "Vorbis audio data")
    static final GstCapsType audioWma = new GstCapsType("audioWma", TypeCategory.AUDIO, "audio/x-wma", "Windows Media Audio")
    static final GstCapsType audioParis = new GstCapsType("audioParis", TypeCategory.AUDIO, "audio/x-paris", "Ensoniq PARIS audio")
    static final GstCapsType audioSvx = new GstCapsType("audioSvx", TypeCategory.AUDIO, "audio/x-svx", "Amiga IFF / SVX8 / SV16 audio")
    static final GstCapsType audioNist = new GstCapsType("audioNist", TypeCategory.AUDIO, "audio/x-nist", "Sphere NIST audio")
    static final GstCapsType audioVoc = new GstCapsType("audioVoc", TypeCategory.AUDIO, "audio/x-voc", "Sound Blaster VOC audio")
    static final GstCapsType audioIrcam = new GstCapsType("audioIrcam", TypeCategory.AUDIO, "audio/x-ircam", "Berkeley/IRCAM/CARL audio")
    static final GstCapsType audioW64 = new GstCapsType("audioW64", TypeCategory.AUDIO, "audio/x-w64", "Sonic Foundry's 64 bit RIFF/WAV")

    static final GstCapsType videoAll = new GstCapsType("videoAll", TypeCategory.VIDEO, "video/*", "All video types")
    static final GstCapsType videoRaw = new GstCapsType("videoRaw", TypeCategory.VIDEO, "video/x-raw", "Unstructured and uncompressed raw video data.")
    static final GstCapsType video3ivx = new GstCapsType("video3ivx", TypeCategory.VIDEO, "video/x-3ivx", "3ivx video.")
    static final GstCapsType videoDivx = new GstCapsType("videoDivx", TypeCategory.VIDEO, "video/x-divx", "DivX video.")
    static final GstCapsType videoDv = new GstCapsType("videoDv", TypeCategory.VIDEO, "video/x-dv", "Digital Video.")
    static final GstCapsType videoFfmpeg = new GstCapsType("videoFfmpeg", TypeCategory.VIDEO, "video/x-ffv", "FFMpeg video.")
    static final GstCapsType videoH263 = new GstCapsType("videoH263", TypeCategory.VIDEO, "video/x-h263", "H-263 video.")
    static final GstCapsType videoH264 = new GstCapsType("videoH264", TypeCategory.VIDEO, "video/x-h264", "H-264 video.")
    static final GstCapsType videoHuffyuy = new GstCapsType("videoHuffyuy", TypeCategory.VIDEO, "video/x-huffyuv", "Huffyuv video.")
    static final GstCapsType videoIndeo = new GstCapsType("videoIndeo", TypeCategory.VIDEO, "video/x-indeo", "Indeo video.")
    static final GstCapsType videoIntelH263 = new GstCapsType("videoIntelH263", TypeCategory.VIDEO, "video/x-intel-h263", "H-263 video.")
    static final GstCapsType videoMjpeg = new GstCapsType("videoMjpeg", TypeCategory.VIDEO, "video/x-jpeg", "Motion-JPEG video.")
    static final GstCapsType videoMpeg = new GstCapsType("videoMpeg", TypeCategory.VIDEO, "video/mpeg", "MPEG video.")
    static final GstCapsType videoMsmpeg = new GstCapsType("videoMsmpeg", TypeCategory.VIDEO, "video/x-msmpeg", "Microsoft MPEG-4 video deviations.")
    static final GstCapsType videoMs = new GstCapsType("videoMs", TypeCategory.VIDEO, "video/x-msvideocodec", "Microsoft Video 1 (oldish codec).")
    static final GstCapsType videoRealmedia = new GstCapsType("videoRealmedia", TypeCategory.VIDEO, "video/x-pn-realvideo", "Realmedia video.")
    static final GstCapsType videoRle = new GstCapsType("videoRle", TypeCategory.VIDEO, "video/x-rle", "RLE animation format.")
    static final GstCapsType videoSvg = new GstCapsType("videoSvg", TypeCategory.VIDEO, "video/x-svq", "Sorensen Video.")
    static final GstCapsType videoTarkin = new GstCapsType("videoTarkin", TypeCategory.VIDEO, "video/x-tarkin", "Tarkin video.")
    static final GstCapsType videoTheora = new GstCapsType("videoTheora", TypeCategory.VIDEO, "video/x-theora", "Theora video.")
    static final GstCapsType videoVp3 = new GstCapsType("videoVp3", TypeCategory.VIDEO, "video/x-vp3", "VP-3 video.")
    static final GstCapsType videoWmv = new GstCapsType("videoWmv", TypeCategory.VIDEO, "video/x-wmv", "Windows Media Video")
    static final GstCapsType videoXvid = new GstCapsType("videoXvid", TypeCategory.VIDEO, "video/x-xvid", "XviD video.")

    static final GstCapsType imageGif = new GstCapsType("imageGif", TypeCategory.IMAGE, "image/gif", "Graphics Interchange Format.")
    static final GstCapsType imageJpeg = new GstCapsType("imageJpeg", TypeCategory.IMAGE, "image/jpeg", "Joint Picture Expert Group Image.")
    static final GstCapsType imagePng = new GstCapsType("imagePng", TypeCategory.IMAGE, "image/png", "Portable Network Graphics Image.")
    static final GstCapsType imageTiff = new GstCapsType("imageTiff", TypeCategory.IMAGE, "image/tiff", "Tagged Image File Format.")

    static final GstCapsType containerAsf = new GstCapsType("containerAsf", TypeCategory.CONTAINER, "video/x-ms-asf", "Advanced Streaming Format (ASF).")
    static final GstCapsType containerAvi = new GstCapsType("containerAvi", TypeCategory.CONTAINER, "video/x-msvideo", "AVI.")
    static final GstCapsType containerDv = new GstCapsType("containerDv", TypeCategory.CONTAINER, "video/x-dv", "Digital Video.")
    static final GstCapsType containerMatroska = new GstCapsType("containerMatroska", TypeCategory.CONTAINER, "video/x-matroska", "Matroska.")
    static final GstCapsType containerMpeg = new GstCapsType("containerMpeg", TypeCategory.CONTAINER, "video/mpeg", "Motion Pictures Expert Group System Stream.")
    static final GstCapsType containerOgg = new GstCapsType("containerOgg", TypeCategory.CONTAINER, "application/ogg", "Ogg.")
    static final GstCapsType containerQuicktime = new GstCapsType("containerQuicktime", TypeCategory.CONTAINER, "video/quicktime", "Quicktime.")
    static final GstCapsType containerRealmedia = new GstCapsType("containerRealmedia", TypeCategory.CONTAINER, "application/vnd.rn-realmedia", "RealMedia.")
    static final GstCapsType containerWav = new GstCapsType("containerWav", TypeCategory.CONTAINER, "audio/x-wav", "WAV.")
}
