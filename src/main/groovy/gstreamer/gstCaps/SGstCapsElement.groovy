package gstreamer.gstCaps

import gstreamer.SGstElement
import gstreamer.gstProperties.*

class SGstCapsElement extends SGstElement {
    GstCapsType type

    // General GstProperties
    GstString format = new GstString("format")

    // Audio GstProperties
    GstInteger channels = new GstInteger("channels")
    GstString layout = new GstString("layout")
    GstInteger blockAlign = new GstInteger("block_align")
    GstBoolean framed = new GstBoolean("framed")
    GstInteger layer = new GstInteger("layer")
    GstInteger bitrate = new GstInteger("bitrate")

    // Video GstProperties
    GstInteger height = new GstInteger("height")
    GstInteger width = new GstInteger("width")
    GstFraction framerate = new GstFraction("framerate")
    GstFraction maxFramerate = new GstFraction("max-framerate")
    GstInteger views = new GstInteger("views")
    GstString interlaceMode = new GstString("interlace-mode")
    GstString chromaSite = new GstString("chroma-site")
    GstString colorimetry = new GstString("colorimetry")
    GstFraction pixelAspectRatio = new GstFraction("pixel-aspect-ratio")
    GstString h263version = new GstString("h263version")
    GstBoolean systemstream = new GstBoolean("systemstream")
    GstInteger depth = new GstInteger("depth")


    SGstCapsElement(GstCapsType type, String name = "") {
        super(GstElementType.TROUGH)
        this.name = name
        this.type = type
    }

    @Override
    String renderElement() {
        def propertiesString = "${this.type.renderValue()}"

        if (this.name != "") {
            propertiesString += ", description=${this.name}"
        }

        this.properties.findAll { it.value instanceof GstProperty && !(it.value instanceof GstCapsType) }.each {
            def element = (GstProperty)it.value
            if (element.wasSet) {
                propertiesString += ", ${element.render()}"
            }
        }
        "\"${propertiesString}\""
    }

}
