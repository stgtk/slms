package gstreamer.gstPlugins


import gstreamer.SGstElement
import gstreamer.gstProperties.GstProperty
import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory

abstract class SGstPlugin extends SGstElement {
    String pluginName
    Element gstElement

    SGstPlugin(String pluginName, GstElementType elementType) {
        super(elementType)
        this.pluginName = pluginName
        this.gstElement = ElementFactory.make(this.pluginName, this.name)
    }

    @Override
    String renderElement() {
        ""
    }

    Element element() {
        this.properties.each {
            if (it.value instanceof GstProperty) {
                def element = (GstProperty) it.value
                if (element.wasSet && element.hasChanged) {
                    this.gstElement.set(element.key, element.value)
                    element.hasChanged = false
                }
            }
        }

        this.gstElement
    }
}
