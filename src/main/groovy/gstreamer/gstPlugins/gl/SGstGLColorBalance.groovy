package gstreamer.gstPlugins.gl


import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstDouble

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-glcolorbalance.html">GStreamer Documentation</a>.
 */
class SGstGLColorBalance extends SGstPlugin {
    GstDouble brightness = new GstDouble("brightness", "Brightness")
    GstDouble contrast = new GstDouble("contrast", "Contrast")
    GstDouble hue = new GstDouble("hue", "Hue")
    GstDouble saturation = new GstDouble("saturation", "Saturation")

    SGstGLColorBalance() {
        super("glcolorbalance", GstElementType.TROUGH)
    }
}
