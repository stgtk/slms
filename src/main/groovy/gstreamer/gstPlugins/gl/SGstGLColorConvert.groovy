package gstreamer.gstPlugins.gl


import gstreamer.gstPlugins.SGstPlugin

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-glcolorconvert.html">GStreamer Documentation</a>.
 */
class SGstGLColorConvert extends SGstPlugin {
    SGstGLColorConvert() {
        super("glcolorconvert", GstElementType.TROUGH)
    }
}
