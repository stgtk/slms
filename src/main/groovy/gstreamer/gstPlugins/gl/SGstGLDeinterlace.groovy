package gstreamer.gstPlugins.gl

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstString

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-gldeinterlace.html">GStreamer Documentation</a>.
 */
class SGstGLDeinterlace extends SGstPlugin {
    GstString method = new GstString("method", "SGstGLDeinterlace. Method Select which deinterlace method apply to GL video texture.")

    SGstGLDeinterlace() {
        super("gldeinterlace", GstElementType.TROUGH)
    }
}
