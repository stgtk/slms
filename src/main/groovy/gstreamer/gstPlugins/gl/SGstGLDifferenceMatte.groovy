package gstreamer.gstPlugins.gl

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstChar

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-gldifferencematte.html">GStreamer Documentation</a>.
 */
class SGstGLDifferenceMatte extends SGstPlugin {
    GstChar location = new GstChar("location", "Background image location")

    SGstGLDifferenceMatte() {
        super("gldifferencematte", GstElementType.TROUGH)
    }
}
