package gstreamer.gstPlugins.gl


import gstreamer.gstPlugins.SGstPlugin

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-gldownload.html">GStreamer Documentation</a>.
 */
class SGstGLDownload extends SGstPlugin {
    SGstGLDownload() {
        super("gldownload", GstElementType.TROUGH)
    }
}
