package gstreamer.gstPlugins.gl

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstBoolean
import gstreamer.gstProperties.GstChar
import gstreamer.gstProperties.GstString

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-glshader.html">GStreamer Documentation</a>.
 */
class SGstGLShader extends SGstPlugin {
    GstChar fragment = new GstChar("fragment", "GLSL fragment source")
    GstString shader = new GstString("shader", "SGstGLShader to use")
    GstBoolean updateShader = new GstBoolean("element-shader", "Emit the 'create-shader' signal for the next frame")
    GstChar vertex = new GstChar("vertex", "GLSL vertex source")

    SGstGLShader() {
        super("glshader", GstElementType.TROUGH)
    }
}
