package gstreamer.gstPlugins.sink

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstBoolean
import gstreamer.gstProperties.GstInteger

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good/html/gst-plugins-good-plugins-autovideosink.html">GStreamer Documentation</a>.
 */
class SGstAutoVideoSink extends SGstPlugin{
    GstInteger tsOffset = new GstInteger("ts-offset", "Timestamp offset in nanoseconds")
    GstBoolean sync = new GstBoolean("sync", "Sync on the clock")

    SGstAutoVideoSink() {
        super("autovideosink", GstElementType.SINK)
    }
}
