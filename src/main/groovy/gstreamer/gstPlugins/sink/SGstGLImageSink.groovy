package gstreamer.gstPlugins.sink


import gstreamer.gstPlugins.SGstPlugin

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-glimagesink.html">GStreamer Documentation</a>.
 */

class SGstGLImageSink extends SGstPlugin {
    SGstGLImageSink() {
        super("glimagesink", GstElementType.SINK)
    }
}
