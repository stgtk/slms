package gstreamer.gstPlugins.sink

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstBoolean
import gstreamer.gstProperties.GstInteger
import gstreamer.gstProperties.GstString

/**
 * For more Information visit the @see <a href="https://developer.ridgerun.com/wiki/index.php?title=GstInterpipe_-_GstInterpipes_Elements_Detailed_Description">GStreamer Documentation</a>.
 */
class SGstInterpipeSink extends SGstPlugin {
    String description
    GstBoolean sync = new GstBoolean("sync", "Sync on the clock")
    GstInteger maxLateness = new GstInteger("max-lateness", "Maximum number of nanoseconds that a buffer can be late before it is dropped (-1 unlimited)")
    GstBoolean qos = new GstBoolean("qos", "Generate Quality-of-Service events upstream")
    GstBoolean async = new GstBoolean("async", "Go asynchronously to PAUSED")
    GstInteger tsOffset = new GstInteger("ts-offset", "Timestamp offset in nanoseconds")
    GstBoolean enableLastSample = new GstBoolean("enable-last-sample", "Enable the last-sample property")
    GstInteger blocksize = new GstInteger("blocksize", "Size in bytes to pull per buffer (0 = default)")
    GstInteger renderDelay = new GstInteger("render-delay", "Additional render delay of the sink in nanoseconds")
    GstInteger throttleTime = new GstInteger("throttle-time", "The time to keep between rendered buffers (0 = disabled)")
    GstInteger maxBitrate = new GstInteger("max-bitrate", "The maximum bits per second to render (0 = disabled)")
    GstString caps = new GstString("caps", "The allowed cpas for the sink pad")
    GstBoolean eos = new GstBoolean("eos", "Check if the sink is EOS or not started")
    GstBoolean emitSignals = new GstBoolean("emit-signals", "Emit new-preroll and new-sample signals")
    GstInteger maxBuffers = new GstInteger("max-buffers", "The maximum number of buffers to queue internally (0 = unlimited)")
    GstBoolean drop = new GstBoolean("drop", "Drop old buffers when the buffer queue is filled")
    GstBoolean forwardEos = new GstBoolean("forward-eos", "Forward the EOS event to all the listeners")
    GstBoolean forwardEvents = new GstBoolean("forward-events", "Forward downstream events to all the listeners (except for EOS)")

    SGstInterpipeSink(String description) {
        super("interpipesink", GstElementType.SINK)
        this.name = UUID.randomUUID()
        this.description = description
    }
}
