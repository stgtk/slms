package gstreamer.gstPlugins.src


import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstBoolean
import gstreamer.gstProperties.GstInteger
import gstreamer.gstProperties.GstString

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-bad/html/gst-plugins-bad-plugins-decklinkvideosrc.html">GStreamer Documentation</a>.
 */
class SGstDecklinkVideoSrc extends SGstPlugin {
    GstInteger bufferSize = new GstInteger("buffer-size", "Size of internal buffer in number of video frames")
    GstString connection = new GstString("connection", "Video input connection to use")
    GstInteger deviceNumber = new GstInteger("device-number", "Output device instance to use")
    GstString mode = new GstString("mode", "Video Mode to use for playback")
    GstString videoFormat = new GstString("video-format", "Video format type to use for input (Only use auto for mode=auto)")
    GstString timecodeFormat = new GstString("timecode-format", "Timecode format type to use for input")
    GstBoolean dropNoSignalFrames = new GstBoolean("drop-no-signal-frames", "Drop frames that are marked as having no input signal")
    GstBoolean outputStreamTime = new GstBoolean("output-stream-time", "Output stream time directly instead of translating to pipeline clock")
    GstInteger skipFirstTime = new GstInteger("skip-first-time", "Skip that much time of initial frames after starting")
    GstBoolean signal = new GstBoolean("signal", "True if there is a valid input signal available")
    GstString hwSerialNumber = new GstString("hw-serial-number", "The serial number (hardware ID) of the Decklink card")

    SGstDecklinkVideoSrc() {
        super("decklinkvideosrc", GstElementType.SRC)
    }
}
