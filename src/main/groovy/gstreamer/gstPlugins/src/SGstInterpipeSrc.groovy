package gstreamer.gstPlugins.src

import gstreamer.gstPlugins.SGstPlugin

/**
 * For more Information visit the @see <a href="https://developer.ridgerun.com/wiki/index.php?title=GstInterpipe_-_GstInterpipes_Elements_Detailed_Description">GStreamer Documentation</a>.
 */
class SGstInterpipeSrc extends SGstPlugin{


    SGstInterpipeSrc() {
        super("interpipesrc", GstElementType.SRC)
    }
}
