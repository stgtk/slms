package gstreamer.gstPlugins.src

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstBoolean
import gstreamer.gstProperties.GstInteger
import gstreamer.gstProperties.GstString

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good/html/gst-plugins-good-plugins-v4l2src.html">GStreamer Documentation</a>.
 */
class SGstV4L2Src extends SGstPlugin {
    GstString device = new GstString("device", "Device location")
    GstString deviceName = new GstString("device-description", "Name of the device")
    GstString flags = new GstString("flags", "Device type flags")
    GstInteger deviceFd = new GstInteger("device-fd", "File descriptor of the device")
    GstInteger brightness = new GstInteger("brightness", "Picture brightness, or more precisely, the black level")
    GstInteger contrast = new GstInteger("contrast", "Picture contrast or luma gain")
    GstInteger hue = new GstInteger("hue", "Hue or color balance")
    GstInteger saturation = new GstInteger("saturation", "Picture color saturation or chroma gain")
    GstString norm = new GstString("norm", "TV norm")
    GstString ioMode = new GstString("io-mode", "IO Mode")
    GstString extraControls = new GstString("extra-controls", "Additional v4l2 controls for the device. The controls are identified by the control description (lowercase with '_' for any non-alphanumeric characters")
    GstBoolean forceAspectRatio = new GstBoolean("force-aspect-ratio", "When enabled, the pixel aspect ratio queried from the device or set with the pixel-aspect-ratio property will be enforced")
    GstString pixelAspectRatio = new GstString("pixel-aspect-ratio", "The pixel aspect ratio of the device. This overwrites the pixel aspect ratio queried from the device")

    SGstV4L2Src() {
        super("v4l2src", GstElementType.SRC)
    }
}
