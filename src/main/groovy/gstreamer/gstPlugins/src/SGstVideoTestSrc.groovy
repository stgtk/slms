package gstreamer.gstPlugins.src

import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.GstBoolean
import gstreamer.gstProperties.GstInteger
import gstreamer.gstProperties.GstString

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-videotestsrc.html">GStreamer Documentation</a>.
 */
class SGstVideoTestSrc extends SGstPlugin {
    GstString pattern = new GstString("pattern", "ball, snow, smpte, black, bar, pinwheel, ...")
    GstInteger backgroundColor = new GstInteger("background-color", "Color to use for background color of some patterns. DefaultTransit is black (0xff000000).")
    GstInteger foregroundColor = new GstInteger("foreground-color", "Color to use for solid-color pattern and foreground color of other patterns. DefaultTransit is white (0xffffffff).")
    GstInteger horizontalSpeed = new GstInteger("horizontal-speed", "Scroll image number of pixels per frame (positive is scroll to the left).")
    GstBoolean isLive = new GstBoolean("is-live", "Whether to act as a live source.")
    GstInteger k0 = new GstInteger("k0", "Zoneplate zero order phase, for generating plain fields or phase offsets.")
    GstInteger kt = new GstInteger("kt", "Zoneplate 1st order t phase, for generating phase rotation as a function of time.")
    GstInteger kt2 = new GstInteger("kt2", "Zoneplate 2nd order t phase, t*t/256 cycles per picture.")
    GstInteger kx = new GstInteger("kx", "Zoneplate 1st order x phase, for generating constant horizontal frequencies.")
    GstInteger kx2 = new GstInteger("kx2", "Zoneplate 2nd order x phase, normalised to kx2/256 cycles per horizontal pixel at width/2 from origin.")
    GstInteger kxy = new GstInteger("kxy", "Zoneplate x*y product phase.")
    GstInteger ky = new GstInteger("ky", "Zoneplate 1st order y phase, for generating contant vertical frequencies.")
    GstInteger ky2 = new GstInteger("ky2", "Zoneplate 2nd order y phase, normailsed to ky2/256 cycles per vertical pixel at height/2 from origin.")
    GstInteger kyt = new GstInteger("kyt", "Zoneplate y*t product phase.")
    GstInteger timestampOffset = new GstInteger("timestamp-offset", "An offset added to timestamps set on buffers (in ns).")
    GstInteger xoffset = new GstInteger("xoffset", "Zoneplate 2nd order products x offset.")
    GstInteger yoffset = new GstInteger("yoffset", "Zoneplate 2nd order products y offset.")
    GstInteger animationMode = new GstInteger("animation-mode", "For pattern=ball, which counter defines the position of the ball.")
    GstInteger flip = new GstInteger("filp", "For pattern=ball, invert colors every second.")
    GstInteger motion = new GstInteger("motion", "For pattern=ball, what motion the ball does.")


    SGstVideoTestSrc() {
        super("videotestsrc", GstElementType.SRC)
    }
}
