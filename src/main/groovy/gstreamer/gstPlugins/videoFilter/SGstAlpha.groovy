package gstreamer.gstPlugins.videoFilter


import gstreamer.gstPlugins.SGstPlugin
import gstreamer.gstProperties.*

/**
 * For more Information visit the @see <a href="https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good/html/gst-plugins-good-plugins-alpha.html">GStreamer Documentation</a>.
 */
class SGstAlpha extends SGstPlugin {
    GstDouble alpha = new GstDouble("alpha", "The value for the alpha channel")
    GstFloat angle = new GstFloat("angle", "Size of the colorcube to change")
    GstString method = new GstString("method", "How the alpha channels should be created")
    GstFloat noiseLevel = new GstFloat("noise-level", "Size of noise radius")
    GstInteger targetB = new GstInteger("target-b", "The blue color value for custom RGB chroma keying")
    GstInteger targetG = new GstInteger("target-g", "The green color value for custom RGB chroma keying")
    GstInteger targetR = new GstInteger("target-r", "The red color value for custom RGB chroma keying")
    GstInteger blackSensitivity = new GstInteger("black-sensitivity", "Sensitivity to dark colors")
    GstInteger whiteSensitivity = new GstInteger("white-sensitivity", "Sensitivity to bright colors")
    GstBoolean preferPassthrough = new GstBoolean("prefer-passthrough", "Don't do any processing for alpha=1.0 if possibleValues")


    SGstAlpha() {
        super("alpha", GstElementType.TROUGH)
    }
}
