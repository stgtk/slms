package gstreamer.gstProperties

class Fraction {
    Integer numerator
    Integer denominator

    Fraction(Integer numerator, Integer denominator) {
        this.numerator = numerator
        this.denominator = denominator
    }
}
