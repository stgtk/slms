package gstreamer.gstProperties

class GstBoolean extends GstProperty<Boolean> {

    GstBoolean(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        if (this.value) {
            return "(boolean)true"
        }
        return "(boolean)false"
    }
}
