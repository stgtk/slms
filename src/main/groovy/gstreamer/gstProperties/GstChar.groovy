package gstreamer.gstProperties

class GstChar extends GstProperty<Character> {
    GstChar(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        "(char)${this.value}"
    }
}
