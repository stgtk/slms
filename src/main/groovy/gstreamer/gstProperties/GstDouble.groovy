package gstreamer.gstProperties

class GstDouble extends GstProperty<Double>{
    GstDouble(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        "(double)${this.value}"
    }
}
