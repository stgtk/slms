package gstreamer.gstProperties

class GstFloat extends GstProperty<Float> {
    GstFloat(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        "(float)${this.value}"
    }
}
