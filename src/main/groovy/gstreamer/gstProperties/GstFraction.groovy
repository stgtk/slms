package gstreamer.gstProperties

class GstFraction extends GstProperty<Fraction> {

    GstFraction(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        return String.format("(fraction)%d/%d", value.numerator, value.denominator)
    }
}
