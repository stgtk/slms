package gstreamer.gstProperties

class GstInteger extends GstProperty<Integer> {

    GstInteger(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        return String.format("(int)%d", this.value)
    }
}
