package gstreamer.gstProperties

abstract class GstProperty<Type> {
    String key
    Type value
    Boolean wasSet = false
    Boolean hasChanged = false
    String description

    GstProperty(String key, String description) {
        this.key = key
        this.description = description
    }

    void setValue(Type value) {
        this.value = value
        this.wasSet = true
        this.hasChanged = true
    }


    String render() {
        return "${this.key}=${this.renderValue()}"
    }

    abstract String renderValue()
}
