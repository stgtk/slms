package gstreamer.gstProperties

class GstString extends GstProperty<String> {

    GstString(String key, String description = "") {
        super(key, description)
    }

    @Override
    String renderValue() {
        return String.format("(string)%s", this.value)
    }
}
