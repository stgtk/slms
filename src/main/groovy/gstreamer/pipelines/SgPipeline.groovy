package gstreamer.pipelines

import gstreamer.gstPlugins.sink.SGstInterpipeSink
import org.freedesktop.gstreamer.Pipeline

abstract class SgPipeline extends Pipeline {
    UUID uuid
    String pipelineType, pipelineDescription, completeName

    SgPipeline(String pipelineType, String pipelineDescription) {
        this.pipelineType = pipelineType
        this.pipelineDescription = pipelineDescription
        this.uuid = UUID.randomUUID()
    }

    String getCompleteName() {
        "${this.pipelineType}-${this.uuid}"
    }

    SGstInterpipeSink[] interpipeSinks() {
        SGstInterpipeSink[] sinks
        this.properties.findAll {it.value instanceof SGstInterpipeSink}.each {
            def sink = (SGstInterpipeSink)it.value
            sinks << sink
        }
        sinks
    }
}
