package gstreamer.pipelines


import gstreamer.gstPlugins.sink.SGstInterpipeSink
import gstreamer.gstPlugins.src.SGstVideoTestSrc

class SgTestCard extends SgPipeline {
    SGstVideoTestSrc videoTestSrc
    SGstInterpipeSink interpipeSink

    SgTestCard() {
        super("videoTestCard", "Simple videotestsrc with the SLMS logo")

        this.videoTestSrc = videoTestSrcFactory()
        this.interpipeSink = interpipeSinkFactory()

        this.addMany(this.videoTestSrc.element(), this.interpipeSink.element())
        this.videoTestSrc.element().link(this.interpipeSink.element())
    }

    private static SGstVideoTestSrc videoTestSrcFactory() {
        def videoTestSrc = new SGstVideoTestSrc()
        videoTestSrc.isLive.value = true
        return videoTestSrc
    }

    private static SGstInterpipeSink interpipeSinkFactory() {
        def interpipeSink = new SGstInterpipeSink("video_sink")
        interpipeSink
    }
}
