package gstreamer.player

import gstreamer.pipelines.SgTestCard
import org.freedesktop.gstreamer.Bus
import org.freedesktop.gstreamer.ElementFactory
import org.freedesktop.gstreamer.Gst
import org.freedesktop.gstreamer.GstObject
import org.freedesktop.gstreamer.Pipeline
import org.freedesktop.gstreamer.lowlevel.MainLoop

class Player {

    static run() {
        Gst.init()
        def pipeline1 = new SgTestCard()
        def pipeline2 = new Pipeline()

        MainLoop loop = new MainLoop()

        def ipSrc = ElementFactory.make("interpipesrc", "ip_src1")
        ipSrc.set("listen-to", pipeline1.interpipeSink.name)
        ipSrc.set("is-live", "true")
        def sink = ElementFactory.make("autovideosink", "thesink")
        pipeline2.addMany(ipSrc, sink)
        ipSrc.link(sink)

        def bus = pipeline2.getBus()
        bus.connect(new Bus.EOS() {
            @Override
            void endOfStream(GstObject source) {
                println("Reached end of stream")
                loop.quit()
            }
        })

        bus.connect(new Bus.ERROR() {

            @Override
            void errorMessage(GstObject source, int code, String message) {
                println("Error. Source: ${source.getName()}, Code: ${code}, Message: ${message}")
            }
        })

        pipeline2.play()
        pipeline1.play()
        loop.run()
    }
}
