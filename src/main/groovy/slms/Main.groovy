package slms

import org.freedesktop.gstreamer.Bin
import org.freedesktop.gstreamer.Gst
import org.freedesktop.gstreamer.lowlevel.MainLoop
import video.sinks.AudioSink
import video.sinks.GlImageSink
import video.sinks.XVImageSink
import video.sources.MJpegWebCamSource
import video.sources.PulseSource
import video.sources.VideoTestSource
import video.transit.DefaultTransit

class Main {
    static void main(String... arg) {
        Gst.init()
        MainLoop loop = new MainLoop()

        def pulseSourcePipeline = new PulseSource("pulse")
        pulseSourcePipeline.device.setValue("alsa_input.usb-Microsoft_Microsoft___LifeCam_Studio_TM_-02.analog-mono")
        def audioSinkPipeline = new AudioSink("sink")
        def webCamSourcePipeline = new MJpegWebCamSource("cam")
        webCamSourcePipeline.devicePath.setValue("/dev/video1")
        def xvImageSinkPipeline = new XVImageSink("video_sink")

        // audioSinkPipeline.listenToSourcePipeline(pulseSourcePipeline)
        xvImageSinkPipeline.listenToSourcePipeline(webCamSourcePipeline)

        pulseSourcePipeline.play()
        audioSinkPipeline.play()
        webCamSourcePipeline.play()
        xvImageSinkPipeline.play()

        pulseSourcePipeline.debugToPdfFile("source")
        audioSinkPipeline.debugToPdfFile("sink")

        /*
        // def videoTestSourcePipeline = new VideoTestSource("source_1", true)
        def webCamPipeline = new MJpegWebCamSource("webcam")
        webCamPipeline.devicePath.setValue("/dev/video1")
        def transitPipeline = new DefaultTransit("transit")
        def xvImageSinkPipeline = new XVImageSink("video_sink")
        def audioSinkPipeline = new AudioSink("audio_sink")

        xvImageSinkPipeline.listenToSourcePipeline(webCamPipeline)
        // audioSinkPipeline.listenToSourcePipeline(transitPipeline)

        // videoTransitPipeline.visibility.setValue(80)

        webCamPipeline.play()
        xvImageSinkPipeline.play()
        // audioSinkPipeline.play()

        webCamPipeline.debugToPdfFile("source")
        transitPipeline.debugToPdfFile("transit")
        xvImageSinkPipeline.debugToPdfFile("video_sink")
        audioSinkPipeline.debugToPdfFile("audio_sink")
        */

        /*
        // def webCamSourcePipeline = new MJpegWebCamSource("source_1")
        // webCamSourcePipeline.devicePath.value = "/dev/video0"
        // def deckLinkSource = new DeckLinkSource("source_1")
        // deckLinkSource.doLink()
        def webCamSourcePipeline = new VideoTestSource("source_1")

        def xvImageSinkPipeline = new XVImageSink("sink_1")
        xvImageSinkPipeline.listenToSourcePipeline(webCamSourcePipeline)
        webCamSourcePipeline.play()
        xvImageSinkPipeline.play()

        webCamSourcePipeline.debugToDotFile(Bin.DEBUG_GRAPH_SHOW_ALL, "deckLink_source")
        xvImageSinkPipeline.debugToDotFile(Bin.DEBUG_GRAPH_SHOW_ALL, "xvimage_sink")
         */

        /*
        // def testSourcePipeline = new VideoTestSource("my_source")
        def camSourcePipeline = new MJpegWebCamSource("web_cam_source")
        camSourcePipeline.devicePath.value = "/dev/video1"
        def glSinkPipeline = new XVImageSink("my_sink")
        glSinkPipeline.listenToSourcePipeline(camSourcePipeline)
        camSourcePipeline.play()
        glSinkPipeline.play()

        camSourcePipeline.debugToDotFile(Bin.DEBUG_GRAPH_SHOW_ALL, "source")
        glSinkPipeline.debugToDotFile(Bin.DEBUG_GRAPH_SHOW_ALL, "sink")
        */

        /*
        Bin bin1 = Gst.parseBinFromDescription("v4l2src device=/dev/video2 ! " +
                "video/x-h264,width=1920,height=1080,framerate=30/1 ! " +
                "h264parse ! " +
                "avdec_h264 ! " +
                "glupload ! " +
                "glcolorconvert ! " +
                "interpipesink name=src1 async=false sync=true", true)

        Bin bin1 = Gst.parseBinFromDescription("" +
                "v4l2src device=/dev/video0 ! " +
                "image/jpeg,framerate=30/1 ! jpegdec ! " +
                "glupload ! " +
                "glcolorconvert ! " +
                "interpipesink name=src1 async=false sync=true caps=\"video/x-raw(memory:GLMemory), format=RGBA, framerate=30/1, width=1920, height=1080\"", true)
        */
        /*
        Bin bin1 = Gst.parseBinFromDescription("" +
                "decklinkvideosrc mode=11 device-number=0 connection=1 name=black ! " +
                " " +
                "glupload ! glcolorconvert ! gldeinterlace ! " +
                "interpipesink name=src1 async=false sync=true caps=\"video/x-raw(memory:GLMemory), format=RGBA, framerate=30/1, width=1920, height=1080\"", true)

        def pipe1 = new Pipeline()
        pipe1.addMany(bin1)
        Pipeline.linkMany(bin1)

        def name = deckLinkSource.videoSinks[0].name
        Bin bin2 = Gst.parseBinFromDescription("" +
                "interpipesrc format=time listen-to=${name} is-live=true enable-sync=true allow-renegotiation=true caps=\"video/x-raw(memory:GLMemory), format=RGBA, framerate=30/1, width=1920, height=1080\" ! " +
                "glimagesink", true)
        def pipe2 = new Pipeline()
        pipe2.addMany(bin2)
        Pipeline.linkMany(bin2)


        deckLinkSource.play()
        pipe2.play()

        deckLinkSource.debugToDotFile(Bin.DEBUG_GRAPH_SHOW_ALL, "pipe1")
        pipe2.debugToDotFile(Bin.DEBUG_GRAPH_SHOW_ALL, "pipe2")
        */

        loop.run()
    }
}
