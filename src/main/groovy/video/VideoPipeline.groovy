package video

import org.freedesktop.gstreamer.Bin
import org.freedesktop.gstreamer.Caps
import org.freedesktop.gstreamer.Pipeline
import video.exceptions.DotConversionFailedException
import video.exceptions.GstDebugDumpDotDirNotSetException
import video.properties.PipelineProperty

import java.nio.file.Path
import java.nio.file.Paths

/**
 * Represents a generic video pipeline.
 *
 * There are three types of Pipelines:
 * - Source: One or more interpipesink's
 * - Trough: One or more interpipesrc's and interpipesink's
 * - Sink: One or more interpipesink's
 *
 * All pipelines accept and/or produce video streams with the following properties:
 * - In GL (video card) Memory
 * - Resolution is FullHD (1920x1080)
 * - 30 Frames per second
 * - Color Mode: RGBA
 */
abstract class VideoPipeline extends Pipeline {
    String description
    final UUID uuid

    /**
     * Constructor. Generates a unique UUID for discriminability.
     *
     * @param description Description for the pipeline. Has not to be unique, primarily for the user.
     */
    VideoPipeline(String description) {
        this.description = description
        this.uuid = UUID.randomUUID()
        this.name = getIdent()
    }

    /**
     * Returns the Ident of the pipeline. Each Ident consists of the description and the uniq UUID.
     *
     * @return Ident of the pipeline.
     */
    String getIdent() {
        return "${this.name}-${this.uuid}"
    }

    PipelineProperty[] getPipelineProperties() {
        PipelineProperty[] properties
        this.properties.findAll { it.value instanceof PipelineProperty }.each {
            def property = (PipelineProperty) it.value
            properties << property
        }
        return properties
    }

    void debugToPdfFile(String fileName) {
        Path dotFilePath, pdfFilePath
        try {
            String dotDirPath = System.getenv("GST_DEBUG_DUMP_DOT_DIR")
            if (dotDirPath == "") {
                throw new GstDebugDumpDotDirNotSetException("The environment variable GST_DEBUG_DUMP_DOT_DIR is not set")
            }

            dotFilePath = Paths.get(dotDirPath, "${fileName}.dot")
            pdfFilePath = Paths.get(dotDirPath, "${fileName}.pdf")
        } catch (GstDebugDumpDotDirNotSetException e) {
            e.printStackTrace()
            return
        }
        this.debugToDotFile(DEBUG_GRAPH_SHOW_ALL, fileName)

        try {
            Process process = Runtime.getRuntime().exec("dot -Tpdf ${dotFilePath} -o ${pdfFilePath}")
            def exitCode = process.waitFor()
            if (exitCode != 0) {
                throw new DotConversionFailedException("Conversation from dot into pdf failed")
            }
        } catch (DotConversionFailedException e) {
            e.printStackTrace()
        }

    }

    static String getResourcePath(Object object, String ressourceName) {
        String raw = object.getClass().getClassLoader().getResource("machine_lullaby.ogg")
        return raw.substring("file:".length())
    }

    static Caps defaultCpuCaps() {
        return new Caps("video/x-raw, format=RGBA, framerate=30/1, width=1920, height=1080")
    }

    static Caps defaultGpuCaps() {
        return new Caps("video/x-raw(memory:GLMemory), format=RGBA, framerate=30/1, width=1920, height=1080")
    }

    static Caps defaultAudioCaps() {
        return new Caps("audio/x-raw, layout=interleaved, rate=48000, format=F32LE, channels=2")
    }
}
