package video.exceptions

class DotConversionFailedException extends Exception {
    DotConversionFailedException(String message) {
        super(message)
    }
}
