package video.exceptions

class GstDebugDumpDotDirNotSetException extends Exception {
    GstDebugDumpDotDirNotSetException(String message) {
        super(message)
    }
}
