package video.exceptions

class PulseCtlListExecutionFailedException extends Exception {
    PulseCtlListExecutionFailedException(String message) {
        super(message)
    }
}
