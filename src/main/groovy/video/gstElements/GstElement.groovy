package video.gstElements

import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import video.VideoPipeline
import video.properties.PipelineProperty

/**
 * Used for all abstraction of GStreamer Elements («Plugins»).
 *
 * Such abstractions are only used if necessary (complex configuration properties, etc.).
 */
class GstElement {
    VideoPipeline videoPipeline
    Element gstElement
    String description
    String name

    /**
     * Constructor of the GstElement
     *
     * @param type GStreamer plugin name
     * @param videoPipeline The pipeline in which the Element is situated.
     * @param description An description of the element.
     */
    GstElement(String type, VideoPipeline videoPipeline, String description) {
        this.videoPipeline = videoPipeline
        this.description = description
        this.gstElement = ElementFactory.make(type, getName())
    }

    /**
     * Getter of the name. Provides an uniq identifier for all elements.
     *
     * @return Uniq name of the element.
     */
    String getName() {
        return "${this.videoPipeline.getIdent()}-${this.description}"
    }

    /**
     * Sets a string property of the shadowed GStreamer element.
     *
     * @param key Element property name.
     * @param value New value of the property.
     * @return Nothing.
     */
    def setElementString(String key, String value) {
        this.gstElement.set(key, value)
    }


    def setElementBoolean(String key, Boolean value) {
        if (value) {
            this.gstElement.set(key, "true")
        } else {
            this.gstElement.set(key, "false")
        }
    }

    /**
     * Sets a bool property of the shadowed GStreamer element.
     *
     * @param key Element property name.
     * @param value New value of the property.
     * @return Nothing.
     */
    def setElementInteger(String key, Integer value) {
        this.gstElement.set(key, value)
    }

    /**
     * Sets a pipeline property to the shadowed GStreamer element.
     * @param pipelineProperty Property to be set.
     * @return Nothing.
     */
    def setElementWithPipelineProperty(PipelineProperty pipelineProperty) {
        this.gstElement.set(pipelineProperty.name, pipelineProperty.value)
    }
}
