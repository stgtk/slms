package video.gstElements

import video.VideoPipeline

class Interpipe extends GstElement {
    enum InterpipeType {
        VIDEO,
        AUDIO
    }

    VideoPipeline videoPipeline
    String type
    String description
    InterpipeType sinkType

    Interpipe(String type, VideoPipeline videoPipeline, String description, InterpipeType interpipeType) {
        super(type, videoPipeline, description)
        this.type = type
        this.description = description
        this.videoPipeline = videoPipeline
        this.sinkType = interpipeType
    }
}
