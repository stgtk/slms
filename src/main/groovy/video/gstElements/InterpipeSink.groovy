package video.gstElements

import video.VideoPipeline

class InterpipeSink extends Interpipe {
    Boolean sync
    Boolean async

    InterpipeSink(VideoPipeline videoPipeline, String description, InterpipeType interpipeType) {
        super("interpipesink", videoPipeline, description, interpipeType)
        this.gstElement.setName(this.name)
        this.sinkType = interpipeType
    }

    void setSync(Boolean sync) {
        this.sync = sync
        this.setElementBoolean("sync", this.sync)
    }

    void setAsync(Boolean async) {
        this.async = async
        this.setElementBoolean("async", this.async)
    }

    static InterpipeSink defaultInterpipeSink(VideoPipeline videoPipeline, String description, InterpipeType interpipeType) {
        def interpipeSink = new InterpipeSink(videoPipeline, description, interpipeType)
        interpipeSink.sync = true
        interpipeSink.async = false
        interpipeSink.gstElement.setCaps(VideoPipeline.defaultGpuCaps())
        return interpipeSink
    }
}
