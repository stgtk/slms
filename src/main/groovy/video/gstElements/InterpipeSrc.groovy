package video.gstElements

import video.VideoPipeline
import video.properties.EnumProperty

class InterpipeSrc extends Interpipe {
    InterpipeSink listenTo
    Boolean isLive
    Boolean allowRenegotiation
    Boolean enableSync
    EnumProperty format = new EnumProperty("format",
            [
                    "undefined": 0,
                    "default": 1,
                    "bytes": 2,
                    "time": 3,
                    "buffers": 4,
                    "percent": 5
            ]
    )

    InterpipeSrc(VideoPipeline videoPipeline, String description, InterpipeType interpipeType) {
        super("interpipesrc", videoPipeline, description, interpipeType)
    }

    void setListenTo(InterpipeSink listenTo) {
        this.listenTo = listenTo
        this.setElementString("listen-to", this.listenTo.name)
    }

    void setIsLive(Boolean isLive) {
        this.isLive = isLive
        this.setElementBoolean("is-live", this.isLive)
    }

    void setAllowRenegotiation(Boolean allowRenegotiation) {
        this.allowRenegotiation = allowRenegotiation
        this.setElementBoolean("allow-renegotiation", this.allowRenegotiation)
    }

    void setEnableSync(Boolean enableSync) {
        this.enableSync = enableSync
        this.setElementBoolean("enable-sync", this.enableSync)
    }

    void setFormat(String format) {
        this.format.setValue(format)
        this.setElementWithPipelineProperty(this.format)
    }

    static InterpipeSrc defaultInterpipeVideoSrc(VideoPipeline videoPipeline, String description) {
        def interpipeSrc = new InterpipeSrc(videoPipeline, description, InterpipeType.VIDEO)
        interpipeSrc.isLive = true
        interpipeSrc.allowRenegotiation = true
        interpipeSrc.enableSync = true
        interpipeSrc.setFormat("time")
        interpipeSrc.gstElement.caps = VideoPipeline.defaultGpuCaps()
        return interpipeSrc
    }

    static InterpipeSrc defaultInterpipeAudioSrc(VideoPipeline videoPipeline, String description) {
        def interpipeSrc = new InterpipeSrc(videoPipeline, description, InterpipeType.AUDIO)
        interpipeSrc.isLive = true
        interpipeSrc.allowRenegotiation = true
        interpipeSrc.enableSync = true
        interpipeSrc.setFormat("time")
        interpipeSrc.gstElement.caps = VideoPipeline.defaultAudioCaps()
        return interpipeSrc
    }
}
