package video.gstElements

import video.VideoPipeline
import video.properties.EnumProperty

class Queue extends GstElement {
    Integer maxSizeBuffers
    EnumProperty leaky = new EnumProperty("leaky",
            [
                    "no": 0,
                    "upstream": 1,
                    "downstream": 2
            ])

    Queue(VideoPipeline videoPipeline, String description) {
        super("queue", videoPipeline, description)
    }

    void setMaxSizeBuffers(Integer maxSizeBuffers) {
        this.maxSizeBuffers = maxSizeBuffers
        this.setElementInteger("max-size-buffers", this.maxSizeBuffers)
    }

    void setLeaky(String leaky) {
        this.leaky.setValue(leaky)
        this.setElementWithPipelineProperty(this.leaky)
    }
}
