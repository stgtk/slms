package video.gstElements

import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory

class SlmsElementFactory {
    static Element AudioConvert(int number) {
        return ElementFactory.make("audioconvert", "audioconvert_${number}")
    }

    static Element AudioResample(int number) {
        return ElementFactory.make("audioresample", "audioresample_${number}")
    }

    static Element AutoAudioSink(int number) {
        return ElementFactory.make("autoaudiosink", "autoaudiosink_${number}")
    }

    static Element AvDecH264(int number) {
        return ElementFactory.make("avdec_h264", "avdec_h264_${number}")
    }

    static Element CapsFilter(int number) {
        return ElementFactory.make("capsfilter", "capsfilter_${number}")
    }

    static Element DeckLinkAudioSink(int number) {
        return ElementFactory.make("decklinkaudiosink", "_${number}")
    }

    static Element DeckLinkAudioSrc(int number) {
        return ElementFactory.make("decklinkaudiosrc", "decklinkaudiosrc_${number}")
    }

    static Element DeckLinkVideoSink(int number) {
        return ElementFactory.make("decklinkvideosink", "decklinkvideosink_${number}")
    }

    static Element DeckLinkVideoSrc(int number) {
        return ElementFactory.make("decklinkvideosrc", "decklinkvideosrc_${number}")
    }

    static Element FileSrc(int number) {
        return ElementFactory.make("filesrc", "filesrc_${number}")
    }

    static Element GlColorBalance(int number) {
        return ElementFactory.make("glcolorbalance", "glcolorbalance_${number}")
    }

    static Element GlColorConvert(int number) {
        return ElementFactory.make("glcolorconvert", "glcolorconvert_${number}")
    }

    static Element GlDownload(int number) {
        return ElementFactory.make("gldownload", "gldownload_${number}")
    }

    static Element GlDeinterlace(int number) {
        return ElementFactory.make("gldeinterlace", "gldeinterlace_${number}")
    }

    static Element GlImageSink(int number) {
        return ElementFactory.make("glimagesink", "glimagesink_${number}")
    }

    static Element GlUpload(int number) {
        return ElementFactory.make("glupload", "glupload_${number}")
    }

    static Element H264Parse(int number) {
        return ElementFactory.make("h264parse", "h264parse_${number}")
    }

    static Element JpegDec(int number) {
        return ElementFactory.make("jpegdec", "jpegdec_${number}")
    }

    static Element OggDemux(int number) {
        return ElementFactory.make("oggdemux", "oggdemux_${number}")
    }

    static Element PulseSrc(int number) {
        return ElementFactory.make("pulsesrc", "pulsesrc_${number}")
    }

    static Element V4L2Src(int number) {
        return ElementFactory.make("v4l2src", "v4l2src_${number}")
    }

    static Element VideoRate(int number) {
        return ElementFactory.make("videorate", "videorate_${number}")
    }

    static Element VideoTestSrc(int number) {
        return ElementFactory.make("videotestsrc", "videotestsrc_${number}")
    }

    static Element VorbisDec(int number) {
        return ElementFactory.make("vorbisdec", "vorbisdec_${number}")
    }

    static Element XVImageSink(int number) {
        return ElementFactory.make("xvimagesink", "xvimagesink_${number}")
    }
}
