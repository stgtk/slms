package video.gstElements

interface VideoProducingPipeline {
    List<InterpipeSink> getVideoSinks()
    List<InterpipeSink> getAudioSinks()
}