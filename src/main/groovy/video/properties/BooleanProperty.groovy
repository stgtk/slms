package video.properties

class BooleanProperty extends PipelineProperty<Boolean> {
    BooleanProperty(String name, String description = "") {
        super(name, description)
    }
}
