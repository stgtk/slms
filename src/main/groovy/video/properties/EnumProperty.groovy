package video.properties


/**
 * A String {@see PipelineProperty} which only allow certain values defined by a set.
 */
class EnumProperty extends PipelineProperty<Integer> {
    Map<String, Integer> map

    EnumProperty(String name, Map<String, Integer> map, String description = "") {
        super(name, description)
        this.map = map
    }

    def setValue(String key) {
        this.value = this.map[key]
    }
}
