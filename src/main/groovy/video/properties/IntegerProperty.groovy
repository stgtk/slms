package video.properties

class IntegerProperty extends PipelineProperty<Integer> {
    IntegerProperty(String name, String description = "") {
        super(name, description)
    }
}
