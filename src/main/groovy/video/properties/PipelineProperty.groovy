package video.properties

import groovy.beans.Bindable

abstract class PipelineProperty<Type> {
    String name
    @Bindable Type value
    String description

    PipelineProperty(String name, String description = "") {
        this.name = name
        this.description = description
    }
}
