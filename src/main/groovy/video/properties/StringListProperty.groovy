package video.properties

class StringListProperty extends StringProperty {
    ArrayList<String> possibleValues

    StringListProperty(String name, ArrayList<String> possibleValues, String description = "") {
        super(name, description)
        this.possibleValues = possibleValues
    }
}
