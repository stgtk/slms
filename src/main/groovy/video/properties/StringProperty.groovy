package video.properties

class StringProperty extends PipelineProperty<String> {
    StringProperty(String name, String description = "") {
        super(name, description)
    }
}
