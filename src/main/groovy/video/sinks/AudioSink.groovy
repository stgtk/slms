package video.sinks

import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import video.gstElements.InterpipeSrc
import video.gstElements.SlmsElementFactory
import video.gstElements.VideoProducingPipeline
import video.sources.SourcePipeline

class AudioSink extends SinkVideoPipeline {
    private InterpipeSrc interpipeSrc
    private Element gstAutoAudioSink

    AudioSink(String description) {
        super(description)
        buildPipeline()
    }

    @Override
    def listenToSourcePipeline(VideoProducingPipeline sourcePipeline) {
        this.interpipeSrc.listenTo = sourcePipeline.audioSinks[0]
    }

    private buildPipeline() {
        this.interpipeSrc = InterpipeSrc.defaultInterpipeAudioSrc(this, "interpipesrc_1")
        this.srcs << this.interpipeSrc
        this.add(this.interpipeSrc.gstElement)

        this.gstAutoAudioSink = SlmsElementFactory.AutoAudioSink(1)
        this.add(this.gstAutoAudioSink)

        this.interpipeSrc.gstElement.link(this.gstAutoAudioSink)
    }
}
