/**
 * This file is part of the Stgtk Live Media System (slms, <https://gitlab.com/stgtk/slms>).
 * Copyright (c) 2019 by Stgkt Bern.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package video.sinks

import video.gstElements.VideoProducingPipeline

/**
 * Sink for Blackmagic DeckLink Cards.
 */
class DeckLinkSink extends SinkVideoPipeline {
    DeckLinkSink(String description) {
        super(description)
    }

    @Override
    def listenToSourcePipeline(VideoProducingPipeline videoPipeline) {
        return null
    }
}
