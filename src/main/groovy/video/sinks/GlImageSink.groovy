package video.sinks

import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import video.gstElements.Interpipe
import video.gstElements.InterpipeSrc
import video.gstElements.VideoProducingPipeline
import video.sources.SourcePipeline

class GlImageSink extends SinkVideoPipeline {
    InterpipeSrc interpipeSrc
    Element gstCapsFilter
    Element gstGlImageSink

    GlImageSink(String description) {
        super(description)
        buildPipeline()
    }

    @Override
    def listenToSourcePipeline(VideoProducingPipeline videoPipeline) {
        this.interpipeSrc.listenTo = videoPipeline.videoSinks[0]
    }

    private buildPipeline() {
        this.interpipeSrc = InterpipeSrc.defaultInterpipeVideoSrc(this, "interpipesrc_1")
        this.srcs << this.interpipeSrc
        this.add(this.interpipeSrc.gstElement)

        this.gstCapsFilter = ElementFactory.make("capsfilter", "capsfilter_1")
        this.gstCapsFilter.setCaps(defaultGpuCaps())
        this.add(this.gstCapsFilter)

        this.gstGlImageSink = ElementFactory.make("glimagesink", "glimagesink_1")
        this.add(this.gstGlImageSink)

        this.interpipeSrc.gstElement.link(this.gstCapsFilter)
        this.gstCapsFilter.link(this.gstGlImageSink)
    }
}
