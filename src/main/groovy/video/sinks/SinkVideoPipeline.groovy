package video.sinks

import video.VideoPipeline
import video.gstElements.InterpipeSrc
import video.gstElements.VideoProducingPipeline

abstract class SinkVideoPipeline extends VideoPipeline {
    List<InterpipeSrc> srcs

    SinkVideoPipeline(String description) {
        super(description)
        this.srcs = []
    }

    abstract listenToSourcePipeline(VideoProducingPipeline videoPipeline)
}
