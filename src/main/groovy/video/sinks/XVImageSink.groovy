package video.sinks

import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import video.gstElements.InterpipeSrc
import video.gstElements.VideoProducingPipeline
import video.properties.BooleanProperty
import video.sources.SourcePipeline

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

class XVImageSink extends SinkVideoPipeline {
    BooleanProperty allowRenegotiation = new BooleanProperty("allowRenegotiation")

    private InterpipeSrc interpipeSrc
    private Element gstGlDownload
    private Element gstXVImageSink

    XVImageSink(String description) {
        super(description)
        buildPipeline()
        addPropertiesListeners()
        allowRenegotiation.value = true
    }

    @Override
    def listenToSourcePipeline(VideoProducingPipeline videoPipeline) {
        this.interpipeSrc.listenTo = videoPipeline.videoSinks[0]
    }

    private buildPipeline() {
        this.interpipeSrc = InterpipeSrc.defaultInterpipeVideoSrc(this, "interpipesrc_1")
        this.srcs << this.interpipeSrc
        this.add(this.interpipeSrc.gstElement)

        this.gstGlDownload = ElementFactory.make("gldownload", "gldownload_1")
        this.add(this.gstGlDownload)

        this.gstXVImageSink = ElementFactory.make("xvimagesink", "xvimagesink_1")
        this.add(this.gstXVImageSink)

        this.interpipeSrc.gstElement.link(this.gstGlDownload)
        this.gstGlDownload.link(this.gstXVImageSink)
    }

    private addPropertiesListeners() {
        this.allowRenegotiation.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                interpipeSrc.allowRenegotiation = propertyChangeEvent.newValue
            }
        })
    }
}
