package video.sources


import org.freedesktop.gstreamer.Caps
import org.freedesktop.gstreamer.Element
import video.gstElements.Interpipe
import video.gstElements.InterpipeSink
import video.gstElements.SlmsElementFactory
import video.properties.EnumProperty
import video.properties.IntegerProperty

class DeckLinkSource extends SourcePipeline {
    private Element gstDeckLinkVideoSrc
    private Element gstVideoRate
    private Element gstVideoRateCaps
    private Element gstGlUpload
    private Element gstGlColorConvert
    private Element gstGlDeinterlace
    private InterpipeSink interpipeVideoSink

    EnumProperty mode
    IntegerProperty deviceNumber
    EnumProperty connection

    DeckLinkSource(String name) {
        super(name)
        this.buildPipeline()
        this.addPropertiesListeners()
        this.doLink()
    }

    private buildPipeline() {
        buildVideoPipeline()
        buildAudioPipeline()
    }

    private buildVideoPipeline() {
        // TODO: GstElement hierfür verwenden
        this.gstDeckLinkVideoSrc = SlmsElementFactory.DeckLinkVideoSrc(1)
        this.gstDeckLinkVideoSrc.set("mode", 11)
        this.gstDeckLinkVideoSrc.set("device-number", 0)
        this.gstDeckLinkVideoSrc.set("connection", 1)
        this.add(this.gstDeckLinkVideoSrc)

        this.gstVideoRate = SlmsElementFactory.VideoRate(1)
        this.add(this.gstVideoRate)

        this.gstVideoRateCaps = SlmsElementFactory.CapsFilter(1)
        this.gstVideoRateCaps.setCaps(new Caps("video/x-raw, framerate=30/1"))
        this.add(this.gstVideoRateCaps)

        this.gstGlUpload = SlmsElementFactory.GlUpload(1)
        this.add(this.gstGlUpload)

        this.gstGlColorConvert = SlmsElementFactory.GlColorConvert(1)
        this.add(this.gstGlColorConvert)

        this.gstGlDeinterlace = SlmsElementFactory.GlDeinterlace(1)
        this.add(this.gstGlDeinterlace)

        this.interpipeVideoSink = InterpipeSink.defaultInterpipeSink(this, "interpipesink_1", Interpipe.InterpipeType.VIDEO)
        this.interpipeVideoSink.gstElement.setCaps(defaultGpuCaps())
        this.videoSinks << this.interpipeVideoSink
        this.add(this.interpipeVideoSink.gstElement)
    }

    private buildAudioPipeline() {
        // TODO: AudioSrc Pipeline hier implementieren (GstElement hierfür verwenden.)
    }

    private addPropertiesListeners() {

    }

    void doLink() {
        this.gstDeckLinkVideoSrc.link(this.gstVideoRate)
        this.gstVideoRate.link(this.gstVideoRateCaps)
        this.gstVideoRateCaps.link(this.gstGlUpload)
        this.gstGlUpload.link(this.gstGlColorConvert)
        this.gstGlColorConvert.link(this.gstGlDeinterlace)
        this.gstGlDeinterlace.link(this.interpipeVideoSink.gstElement)
    }

    EnumProperty getMode() {
        return this.deckLinkVideoSrc.mode
    }

    IntegerProperty getDeviceNumber() {
        return this.deckLinkVideoSrc.deviceNumber
    }

    EnumProperty getConnection() {
        return this.deckLinkVideoSrc.connection
    }

    void setMode(String mode) {
        this.deckLinkVideoSrc.setMode(mode)
    }

    void setDeviceNumber(Integer deviceNumber) {
        this.deckLinkVideoSrc.setDeviceNumber(deviceNumber)
    }

    void setConnection(String connection) {
        this.deckLinkVideoSrc.setConnection(connection)
    }
}
