package video.sources

import org.freedesktop.gstreamer.Caps
import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import video.gstElements.Interpipe
import video.gstElements.InterpipeSink
import video.gstElements.SlmsElementFactory
import video.properties.StringProperty

class H264WebCamSource extends SourcePipeline {
    StringProperty devicePath = new StringProperty("Device Path")

    private Element gstV4L2Src
    private Element gstWebCamCaps
    private Element gstH264Parse
    private Element gstAvDecH264
    private Element gstGlUpload
    private Element gstGlColorConvert
    private InterpipeSink interpipeVideoSink

    H264WebCamSource(String name) {
        super(name)
        this.buildPipeline()
        this.addPropertiesListeners()
    }

    private buildPipeline() {
        this.gstV4L2Src = ElementFactory.make("v4l2src", "v4l2src_1")
        this.devicePath.value = "/dev/video0"
        this.add(this.gstV4L2Src)

        this.gstWebCamCaps = SlmsElementFactory.CapsFilter(1)
        this.gstWebCamCaps.setCaps(new Caps("video/x-h264, width=1920, height=1080, framerate=30/1"))
        this.add(this.gstWebCamCaps)

        this.gstH264Parse = SlmsElementFactory.H264Parse(1)
        this.add(this.gstH264Parse)

        this.gstAvDecH264 = SlmsElementFactory.AvDecH264(1)
        this.add(this.gstAvDecH264)

        this.gstGlUpload = SlmsElementFactory.GlUpload(1)
        this.add(this.gstGlUpload)

        this.gstGlColorConvert = SlmsElementFactory.GlColorConvert(1)
        this.add(this.gstGlColorConvert)

        this.interpipeVideoSink = InterpipeSink.defaultInterpipeSink(this, "interpipesink_1", Interpipe.InterpipeType.VIDEO)
        this.interpipeVideoSink.gstElement.setCaps(defaultGpuCaps())
        this.videoSinks << this.interpipeVideoSink
        this.add(this.interpipeVideoSink.gstElement)
    }

    private addPropertiesListeners() {

    }
}
