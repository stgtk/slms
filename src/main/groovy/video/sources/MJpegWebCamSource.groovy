package video.sources

import org.freedesktop.gstreamer.Caps
import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import video.gstElements.Interpipe
import video.gstElements.InterpipeSink
import video.properties.StringProperty

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

/**
 * H264 Web Cam Input.
 * `gst-launch-1.0 v4l2src device=/dev/video0 ! jpegdec ! glupload ! glcolorconvert ! gleffects_sin ! glimagesink`
 */
class MJpegWebCamSource extends SourcePipeline {
    StringProperty devicePath = new StringProperty("Device Path")

    private Element gstV4l2Src
    private Element gstJpegCaps
    private Element gstJpegDec
    private Element gstGlUpload
    private Element gstGlColorConvert
    private InterpipeSink interpipeVideoSink

    MJpegWebCamSource(String name) {
        super(name)
        buildPipeline()
        addPropertiesListeners()
    }

    private buildPipeline() {
        this.gstV4l2Src = ElementFactory.make("v4l2src", "v4l2src_1")
        this.devicePath.value = "/dev/video0"
        this.add(this.gstV4l2Src)

        this.gstJpegCaps = ElementFactory.make("capsfilter", "capsfilter_1")
        this.gstJpegCaps.setCaps(new Caps("image/jpeg"))
        this.add(this.gstJpegCaps)

        this.gstJpegDec = ElementFactory.make("jpegdec", "jpegdec_1")
        this.add(this.gstJpegDec)

        this.gstGlUpload = ElementFactory.make("glupload", "glupload_1")
        this.add(this.gstGlUpload)

        this.gstGlColorConvert = ElementFactory.make("glcolorconvert", "glcolorconvert_1")
        this.add(this.gstGlColorConvert)

        this.interpipeVideoSink = InterpipeSink.defaultInterpipeSink(this, "interpipesink_1", Interpipe.InterpipeType.VIDEO)
        this.interpipeVideoSink.gstElement.setCaps(defaultGpuCaps())
        this.videoSinks << this.interpipeVideoSink
        this.add(this.interpipeVideoSink.gstElement)

        this.gstV4l2Src.link(this.gstJpegCaps)
        this.gstJpegCaps.link(this.gstJpegDec)
        this.gstJpegDec.link(this.gstGlUpload)
        this.gstGlUpload.link(this.gstGlColorConvert)
        this.gstGlColorConvert.link(this.interpipeVideoSink.gstElement)
    }

    private addPropertiesListeners() {
        this.devicePath.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                gstV4l2Src.set("device", propertyChangeEvent.newValue)

            }
        })
    }
}
