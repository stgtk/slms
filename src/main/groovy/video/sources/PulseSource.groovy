package video.sources

import org.freedesktop.gstreamer.Element
import video.exceptions.PulseCtlListExecutionFailedException
import video.gstElements.Interpipe
import video.gstElements.InterpipeSink
import video.gstElements.SlmsElementFactory
import video.properties.StringListProperty

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener
import java.util.regex.Matcher
import java.util.regex.Pattern

class PulseSource extends SourcePipeline {
    StringListProperty device

    private Element gstPulseSrc
    private Element gstAudioConvert
    private Element gstAudioResample
    private Element gstAudioCapsFilter
    private Element bla
    private InterpipeSink interpipeAudioSink

    PulseSource(String name) {
        super(name)
        this.device = new StringListProperty("device", getPossibleDevices())
        buildPipeline()
        addPropertiesListeners()
        doLink()

    }

    private buildPipeline() {
        this.gstPulseSrc = SlmsElementFactory.PulseSrc(1)
        this.add(this.gstPulseSrc)

        this.gstAudioConvert = SlmsElementFactory.AudioConvert(1)
        this.add(this.gstAudioConvert)

        this.gstAudioResample = SlmsElementFactory.AudioResample(1)
        this.add(this.gstAudioResample)

        this.gstAudioCapsFilter = SlmsElementFactory.CapsFilter(1)
        this.gstAudioCapsFilter.setCaps(defaultAudioCaps())
        this.add(this.gstAudioCapsFilter)

        this.bla = SlmsElementFactory.AutoAudioSink(1)
        this.add(this.bla)

        this.interpipeAudioSink = InterpipeSink.defaultInterpipeSink(this, "intperipesink_1", Interpipe.InterpipeType.AUDIO)
        this.interpipeAudioSink.gstElement.setCaps(defaultAudioCaps())
        this.audioSinks << this.interpipeAudioSink
        this.add(this.interpipeAudioSink.gstElement)
    }

    private addPropertiesListeners() {
        this.device.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                gstPulseSrc.set("device", propertyChangeEvent.newValue)
            }
        })
    }

    private doLink() {
        linkPads(this.gstPulseSrc, "src", this.gstAudioConvert, "sink")
        this.gstAudioConvert.link(this.gstAudioResample)
        this.gstAudioResample.link(this.gstAudioCapsFilter)
        this.gstAudioCapsFilter.link(this.bla)
    }

    private static ArrayList<String> getPossibleDevices() throws PulseCtlListExecutionFailedException {
        Process process = Runtime.getRuntime().exec("pactl list")
        int resultCode = process.waitFor()

        if (resultCode != 0) {
            throw new PulseCtlListExecutionFailedException("The execution of pulse audio ctl failed.")
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))
        String line
        def output = ""
        while ((line = reader.readLine()) != null) {
            output = "$output\n$line"
        }

        Matcher matcher = Pattern.compile('Source #\\d\\n\\s.*\\n\\sName: (.*)').matcher(output)
        def result = new ArrayList()
        while (matcher.find()) {
            result.add(matcher.group(1))
        }

        return result
    }
}
