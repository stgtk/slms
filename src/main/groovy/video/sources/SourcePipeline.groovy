package video.sources

import video.VideoPipeline
import video.gstElements.InterpipeSink
import video.gstElements.VideoProducingPipeline

abstract class SourcePipeline extends VideoPipeline implements VideoProducingPipeline {
    List<InterpipeSink> videoSinks
    List<InterpipeSink> audioSinks

    SourcePipeline(String name) {
        super(name)
        this.videoSinks = []
        this.audioSinks = []
    }
}
