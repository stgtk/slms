package video.sources

import org.freedesktop.gstreamer.Element
import org.freedesktop.gstreamer.ElementFactory
import org.freedesktop.gstreamer.Pad
import video.gstElements.Interpipe
import video.gstElements.InterpipeSink
import video.gstElements.SlmsElementFactory
import video.properties.EnumProperty

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

/**
 * A Test Video Source.
 *
 * Per default it shows ths SMTP test card.
 *
 * videotestsrc --> capsfilter --> glupload --> glcolorconvert --> interpipesink
 */
class VideoTestSource extends SourcePipeline {
    EnumProperty pattern = new EnumProperty("pattern",
            [
                    "smpte"            : 0,
                    "snow"             : 1,
                    "black"            : 2,
                    "white"            : 3,
                    "red"              : 4,
                    "green"            : 5,
                    "blue"             : 6,
                    "checkers-1"       : 7,
                    "checkers-2"       : 8,
                    "checkers-4"       : 9,
                    "checkers-8"       : 10,
                    "circular"         : 11,
                    "blink"            : 12,
                    "smpte75"          : 13,
                    "zone-plate"       : 14,
                    "gamut"            : 15,
                    "chroma-zone-plate": 16,
                    "solid-color"      : 17,
                    "ball"             : 18,
                    "smpte100"         : 19,
                    "bar"              : 20,
                    "pinwheel"         : 21,
                    "spokes"           : 22,
                    "gradient"         : 23,
                    "colors"           : 24
            ],
            "Possible test patterns")
    Boolean withAudio

    private Element gstVideoTestSrc
    private Element gstVideoCapsFilter
    private Element gstGlUpload
    private Element gstGlColorConvert
    private InterpipeSink interpipeVideoSink

    private Element gstFileSrc
    Element gstOggDemux
    private Element gstVorbisDec
    private Element gstAudioConvert
    private Element gstAudioResample
    private Element gstAudioCapsFilter
    private InterpipeSink interpipeAudioSink

    VideoTestSource(String description, Boolean withAudio) {
        super(description)
        this.withAudio = withAudio
        buildPipeline()
        addPropertiesListeners()
    }

    private buildPipeline() {
        this.buildVideoPipeline()
        if (this.withAudio) {
            this.buildAudioPipeline()
        }
    }

    private buildVideoPipeline() {
        this.gstVideoTestSrc = SlmsElementFactory.VideoTestSrc(1)
        this.gstVideoTestSrc.set("is-live", "true")
        this.pattern.setValue("smpte")
        this.add(this.gstVideoTestSrc)

        this.gstVideoCapsFilter = SlmsElementFactory.CapsFilter(1)
        this.gstVideoCapsFilter.setCaps(defaultCpuCaps())
        this.add(this.gstVideoCapsFilter)

        this.gstGlUpload = ElementFactory.make("glupload", "glupload_1")
        this.add(this.gstGlUpload)

        /*
        this.queue = new Queue(this, "queue_1")
        this.queue.maxSizeBuffers = 3
        this.queue.leaky = "downstream"
        this.add(this.queue.gstElement)
        */

        this.gstGlColorConvert = SlmsElementFactory.GlColorConvert(1)
        this.add(this.gstGlColorConvert)

        this.interpipeVideoSink = InterpipeSink.defaultInterpipeSink(this, "interpipesink_1", Interpipe.InterpipeType.VIDEO)
        this.interpipeVideoSink.gstElement.setCaps(defaultGpuCaps())
        this.videoSinks << this.interpipeVideoSink
        this.add(this.interpipeVideoSink.gstElement)

        this.gstVideoTestSrc.link(this.gstVideoCapsFilter)
        this.gstVideoCapsFilter.link(this.gstGlUpload)
        this.gstGlUpload.link(this.gstGlColorConvert)
        this.gstGlColorConvert.link(this.interpipeVideoSink.gstElement)
    }

    private buildAudioPipeline() {
        this.gstFileSrc = SlmsElementFactory.FileSrc(1)
        this.gstFileSrc.set("location", getResourcePath(this, "machine_lullaby.ogg"))
        this.add(this.gstFileSrc)

        this.gstOggDemux = SlmsElementFactory.OggDemux(1)
        this.add(this.gstOggDemux)

        this.gstVorbisDec = SlmsElementFactory.VorbisDec(1)
        this.add(this.gstVorbisDec)

        this.gstAudioConvert = SlmsElementFactory.AudioConvert(1)
        this.add(this.gstAudioConvert)

        this.gstAudioResample = SlmsElementFactory.AudioResample(1)
        this.add(this.gstAudioResample)

        this.gstAudioCapsFilter = SlmsElementFactory.CapsFilter(2)
        this.gstAudioCapsFilter.setCaps(defaultAudioCaps())
        this.add(this.gstAudioCapsFilter)

        this.interpipeAudioSink = InterpipeSink.defaultInterpipeSink(this, "interpipesink_2", Interpipe.InterpipeType.AUDIO)
        this.interpipeAudioSink.gstElement.setCaps(defaultAudioCaps())
        this.audioSinks << this.interpipeAudioSink
        this.add(this.interpipeAudioSink.gstElement)

        linkPads(this.gstFileSrc, "src", this.gstOggDemux, "sink")

        this.gstOggDemux.connect(new Element.PAD_ADDED() {
            @Override
            void padAdded(Element element, Pad pad) {
                gstOggDemux.link(gstVorbisDec)
                gstVorbisDec.link(gstAudioConvert)
                gstAudioConvert.link(gstAudioResample)
                gstAudioResample.link(gstAudioCapsFilter)
                gstAudioCapsFilter.link(interpipeAudioSink.gstElement)
            }
        })
    }

    private addPropertiesListeners() {
        this.pattern.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                gstVideoTestSrc.set("pattern", propertyChangeEvent.newValue)
            }
        })
    }
}
