package video.transit

import org.freedesktop.gstreamer.Element
import video.gstElements.Interpipe
import video.gstElements.InterpipeSink
import video.gstElements.InterpipeSrc
import video.gstElements.SlmsElementFactory
import video.gstElements.SlmsPipeline
import video.gstElements.VideoProducingPipeline
import video.properties.BooleanProperty
import video.properties.IntegerProperty

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

class DefaultTransit extends TransitPipeline implements SlmsPipeline {
    IntegerProperty visibility = new IntegerProperty("visibility", description = "Value between 0 (black) and 100 (image)")
    BooleanProperty isMonochromatic = new BooleanProperty("isMonochromatic")

    private InterpipeSrc interpipeVideoSrc
    private Element gstGlColorBalance
    private InterpipeSink interpipeVideoSink

    private InterpipeSrc interpipeAudioSrc
    private InterpipeSink interpipeAudioSink
    private Boolean doAudio

    DefaultTransit(String description) {
        super(description)

        buildPipeline()
        addPropertiesListeners()
        doLinking()
    }

    @Override
    def listenToSourcePipeline(VideoProducingPipeline videoPipeline) {
        this.interpipeVideoSrc.listenTo = videoPipeline.videoSinks[0]


        if (videoPipeline.audioSinks.size() > 0) {
            this.doAudio = true
            this.interpipeAudioSrc.listenTo = videoPipeline.audioSinks[0]
        } else {
            teardownAudioPipeline()
        }
    }

    private buildPipeline() {
        buildVideoPipeline()
        buildAudioPipeline()
    }

    private buildVideoPipeline() {
        this.interpipeVideoSrc = InterpipeSrc.defaultInterpipeVideoSrc(this, "interpipesrc_1")
        this.videoSrcs << this.interpipeVideoSrc
        this.add(this.interpipeVideoSrc.gstElement)

        this.gstGlColorBalance = SlmsElementFactory.GlColorBalance(1)
        this.gstGlColorBalance.set("saturation", 0.5)
        this.add(this.gstGlColorBalance)

        this.interpipeVideoSink = new InterpipeSink(this, "interpipesink_1", Interpipe.InterpipeType.VIDEO)
        this.interpipeVideoSink.gstElement.setCaps(defaultGpuCaps())
        this.videoSinks << this.interpipeVideoSink
        this.add(this.interpipeVideoSink.gstElement)
    }

    private buildAudioPipeline() {
        this.interpipeAudioSrc = InterpipeSrc.defaultInterpipeAudioSrc(this, "interpipesrc_2")
        this.audioSrcs << this.interpipeAudioSrc
        this.add(this.interpipeAudioSrc.gstElement)

        this.interpipeAudioSink = new InterpipeSink(this, "interpipesink_2", Interpipe.InterpipeType.AUDIO)
        this.interpipeAudioSink.gstElement.setCaps(defaultAudioCaps())
        this.audioSinks << this.interpipeAudioSink
        this.add(this.interpipeAudioSink.gstElement)
    }

    private teardownAudioPipeline() {
        this.interpipeAudioSrc.gstElement.unlink(this.interpipeAudioSink.gstElement)

        this.remove(this.interpipeAudioSrc.gstElement)
        this.remove(this.interpipeAudioSink.gstElement)
    }

    @Override
    void doLinking() {
        this.interpipeVideoSrc.gstElement.link(this.gstGlColorBalance)
        this.gstGlColorBalance.link(this.interpipeVideoSink.gstElement)

        this.interpipeAudioSrc.gstElement.link(this.interpipeAudioSink.gstElement)
    }

    private addPropertiesListeners() {
        this.visibility.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Double brightness = ((Integer) propertyChangeEvent.newValue - 100) / 100

                Double saturation
                if (isMonochromatic.value) {
                    saturation = 0.0
                } else {
                    saturation = (Integer) propertyChangeEvent.newValue / 100
                }

                println("Brighntess: ${brightness}, Saturation: ${saturation}")

                gstGlColorBalance.set("brightness", brightness)
                gstGlColorBalance.set("saturation", saturation)
            }
        })

        this.isMonochromatic.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                gstGlColorBalance.set("saturation", 0)
            }
        })
    }

    void fadeIn(Integer duration) {

    }

    void fadeOut(Integer duration) {

    }
}
