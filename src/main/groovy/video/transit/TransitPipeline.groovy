package video.transit

import video.VideoPipeline
import video.gstElements.InterpipeSink
import video.gstElements.InterpipeSrc
import video.gstElements.VideoProducingPipeline

abstract class TransitPipeline extends VideoPipeline implements VideoProducingPipeline {
    List<InterpipeSrc> videoSrcs
    List<InterpipeSrc> audioSrcs
    List<InterpipeSink> videoSinks
    List<InterpipeSink> audioSinks

    TransitPipeline(String description) {
        super(description)
        this.videoSrcs = []
        this.audioSrcs = []
        this.videoSinks = []
        this.audioSinks = []
    }

    abstract listenToSourcePipeline(VideoProducingPipeline videoPipeline)
}
